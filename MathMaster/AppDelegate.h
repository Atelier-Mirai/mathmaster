//
//  MathMasterAppDelegate.h
//  MathMaster
//
//  Created by 廣瀬 誠 on 平成23/11/18.
//  Copyright 23 岐阜IT協同組合. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@class MainViewController;
@class RecordViewController;

@interface AppDelegate : NSObject <UIApplicationDelegate> {
	
	
}

@property (assign) BOOL isJapanease;

@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@property (nonatomic, retain) IBOutlet MainViewController *mainViewController;

- (void) confirmLanguage;

@end
