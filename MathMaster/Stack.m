//
//  Stack.m
//  Mental ArithmeticKind Master
//
//  Created by 廣瀬 誠 on 平成23/11/17.
//  Copyright 23 岐阜IT協同組合. All rights reserved.
//

#import "Stack.h"


@implementation NSMutableArray (Stack)

- (id) pop {
	//nil if [self count] == 0
	id lastObject = [[[self lastObject] retain] autorelease];
	if (lastObject){
		[self removeLastObject];
	}
	return lastObject;
}

- (id) peek {
	id lastObject = [[[self lastObject] retain] autorelease];
	return lastObject;
}

- (void)push:(id)obj {
	[self addObject:obj];
}

- (int) length {
	return [self count];
}


@end
