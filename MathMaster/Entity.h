//
//  Entity.h
//  MathMaster
//
//  Created by 廣瀬 誠 on 平成23/11/25.
//  Copyright (c) 23 岐阜IT協同組合. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Entity : NSManagedObject {
@private
}
@property (nonatomic, retain) NSNumber * digitOfNumbers;
@property (nonatomic, retain) NSNumber * arithmeticKind;
@property (nonatomic, retain) NSNumber * clearTime;
@property (nonatomic, retain) NSDate * clearDate;
@property (nonatomic, retain) NSNumber * helpLevel;

@end
