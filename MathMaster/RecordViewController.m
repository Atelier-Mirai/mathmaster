//
//  RecordViewController.m
//  Mental Arithmetic Master
//
//  Created by 廣瀬 誠 on 平成23/11/10.
//  Copyright 23 岐阜IT協同組合. All rights reserved.
//

#import "RecordViewController.h"
#import "Entity.h"


@implementation RecordViewController

//アプリケーションデリゲート
@synthesize appDelegate;

//IB Outlet
@synthesize kindSegmentedControl;
@synthesize labels;

//CoreData用
@synthesize managedObjectContext;
@synthesize eventsArray;


@synthesize delegate=_delegate;


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

#pragma mark - View lifecycle
int indexToTag[RECORD_COLUMNS * RECORD_MAX + 1];		// 30+1
int tagToIndex[RECORD_MAX * 10 + RECORD_COLUMNS + 1];	//103+1



- (void)viewDidLoad
{
    [super viewDidLoad];
	self.view.backgroundColor = [UIColor viewFlipsideBackgroundColor]; 

	//使えるフォントの一覧
//	NSLog(@"%@", [UIFont familyNames]);
	
	//初期化
//	labels = [[NSMutableArray alloc] initWithCapacity:1];
	//nibでOutletCollectionとして登録済みなので、不要
	
	//tagとlabels配列の添字との対応表作成
	for (int i = 0; i < RECORD_COLUMNS * RECORD_MAX + 1; i++) indexToTag[i] = VACANT;
	for (int i = 0; i < RECORD_MAX * 10 + RECORD_COLUMNS + 1; i++) tagToIndex[i] = VACANT;
	UILabel *label = nil;
	for (int i = 0; i < RECORD_COLUMNS * RECORD_MAX; i++){
		label = [labels objectAtIndex:i];
		indexToTag[i] = label.tag;
		tagToIndex[label.tag] = i;
	}

	//記録ラベルの初期化
	[self setNoRecordToLabel];

	//
	//アプリケーションデリゲートポインタを取得
	//
	appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	managedObjectContext = appDelegate.managedObjectContext;

	
	//順番は並び替えされているけれども、
	//全件、フェッチされているはず。
	//表示
	[self displayRecord];

}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Actions

- (IBAction)back:(id)sender
{
    [self.delegate recordViewControllerDidFinish:self];
}

#pragma mark - Reset

//CoreData消去
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 1){	//		NSLog(@"消去します");
		
		
		int kind = kindSegmentedControl.selectedSegmentIndex;
		
		//ここから本番
		//フェッチ要求の作成
		NSFetchRequest *request = [[NSFetchRequest alloc] init];
		NSEntityDescription *entity = [NSEntityDescription entityForName:@"RecordEntity" inManagedObjectContext:managedObjectContext];
		[request setEntity:entity];
		
		//抽出条件をセット
		NSPredicate *predicate = [NSPredicate predicateWithFormat: 
								  [NSString stringWithFormat:@"arithmeticKind == %d",kind]]; 	
		[request setPredicate:predicate];
		
		
		//ソート記述子の設定
		NSSortDescriptor *sortDescriptorTime = [[NSSortDescriptor alloc] initWithKey:@"clearTime" ascending:YES];
		NSSortDescriptor *sortDescriptorDate = [[NSSortDescriptor alloc] initWithKey:@"clearDate" ascending:YES];
		NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorTime, sortDescriptorDate, nil];
		//先にtimeを記述しているので、time, dateの順でソートされる。
		[request setSortDescriptors:sortDescriptors];
		
		[sortDescriptors release];
		[sortDescriptorTime release];
		[sortDescriptorDate release];
		
		//10件だけ抽出する
//		[request setFetchLimit:RECORD_MAX];
		
		//フェッチ要求の実行
		NSError *error = nil;
		NSMutableArray *mutableFetchResults = [[managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
		if (mutableFetchResults == nil) {
			// エラーを処理する
		}
		
		//	//件数を出してみる。
		//	NSLog(@"mutable count %d",[mutableFetchResults count]);
		
		//終了処理
		[self setEventsArray:mutableFetchResults];	//ソートされたフェッチ結果が格納される
		[mutableFetchResults release];
		[request release];	


		// 指定のインデックスパスにある管理オブジェクトを削除する。
//		NSLog(@"count mae %d", [eventsArray count]);
		while ([eventsArray count]){
//			NSLog(@"count naka %d", [eventsArray count]);
			NSManagedObject *eventToDelete = [eventsArray objectAtIndex:0];
			[managedObjectContext deleteObject:eventToDelete];
			// 配列とViewを更新する。
			[eventsArray removeObjectAtIndex:0];
		
			// 変更をコミットする。
			NSError *error = nil;
			if (![managedObjectContext save:&error]) {
				// エラーを処理する。
			}
		}
//		NSLog(@"count ato %d", [eventsArray count]);
		
		//再描画
		[self setNoRecordToLabel];
		
	}
}

- (IBAction)reset:(id)sender {
	//アラート表示
	UIAlertView* alert = [[UIAlertView alloc] initWithTitle: nil
													message: NSLocalizedString(@"Reset Records?", nil)
												   delegate: self
										  cancelButtonTitle: NSLocalizedString(@"Cancel", nil)
										  otherButtonTitles: NSLocalizedString(@"OK", nil), nil];
	[alert show];
	[alert release];
}

- (void) setNoRecordToLabel {
	NSString *timeString;
	NSString *digitString;
	NSString *dateString;

	UILabel *label;

	for (int i = 0; i < RECORD_MAX; i++){ 
		timeString = @"--.---";
		label = [labels objectAtIndex:tagToIndex[(i+1)*10 + 1]];
		label.textColor = [UIColor whiteColor];
		label.text = timeString;
		
		label.font = [UIFont fontWithName:@"Courier New" size:12];
		
		digitString = @"-";
		label = [labels objectAtIndex:tagToIndex[(i+1)*10 + 2]];
		label.textColor = [UIColor whiteColor];
		label.text = digitString;
		label.font = [UIFont fontWithName:@"Courier New" size:12];
		
		dateString = @"----/--/-- --:--:--   ";
		label = [labels objectAtIndex:tagToIndex[(i+1)*10 + 3]];
		label.textColor = [UIColor whiteColor];
		label.text = dateString;
		label.font = [UIFont fontWithName:@"Courier New" size:12];
		
	}
}



#pragma mark - showRecord

//演算子の種類kindに応じた記録を表示する
- (void) displayRecord {
	
	//演算子の種類が選択されていないなら、
	//ユーザーデフォルトから読み込み、セグメントコントロールに選択済みとしてセットする
	int kind;
	if (kindSegmentedControl.selectedSegmentIndex == -1){
		NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
		kindSegmentedControl.selectedSegmentIndex = [userDefaults integerForKey:@"ARITHMETIC_KIND"];
	}
	kind = kindSegmentedControl.selectedSegmentIndex;

	//ここから本番
	//フェッチ要求の作成
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"RecordEntity" inManagedObjectContext:managedObjectContext];
	[request setEntity:entity];
	
	//抽出条件をセット
	NSPredicate *predicate = [NSPredicate predicateWithFormat: 
							  [NSString stringWithFormat:@"arithmeticKind == %d",kind]]; 	
	[request setPredicate:predicate];
	
	
	//ソート記述子の設定
	NSSortDescriptor *sortDescriptorTime = [[NSSortDescriptor alloc] initWithKey:@"clearTime" ascending:YES];
	NSSortDescriptor *sortDescriptorDate = [[NSSortDescriptor alloc] initWithKey:@"clearDate" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorTime, sortDescriptorDate, nil];
	//先にtimeを記述しているので、time, dateの順でソートされる。
	[request setSortDescriptors:sortDescriptors];
	
	[sortDescriptors release];
	[sortDescriptorTime release];
	[sortDescriptorDate release];
	
	//10件だけ抽出する & 溢れた記録があるかもしれないので＋1している
	[request setFetchLimit:RECORD_MAX + 1];

	//フェッチ要求の実行
	NSError *error = nil;
	NSMutableArray *mutableFetchResults = [[managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
	if (mutableFetchResults == nil) {
		// エラーを処理する
	}
	
	//終了処理
	[self setEventsArray:mutableFetchResults];	//ソートされたフェッチ結果が格納される
	[mutableFetchResults release];
	[request release];	

	//保存している記録が10件超過していたら、その分（きっと一つ）を削除する
	// 指定のインデックスパスにある管理オブジェクトを削除する。
	while ([eventsArray count] > RECORD_MAX){
		NSManagedObject *eventToDelete = [eventsArray objectAtIndex:RECORD_MAX];
		[managedObjectContext deleteObject:eventToDelete];
		// 配列とViewを更新する。
		[eventsArray removeObjectAtIndex:RECORD_MAX];
		
		// 変更をコミットする。
		NSError *error = nil;
		if (![managedObjectContext save:&error]) {
			// エラーを処理する。
		}
	}
	
	//以下、フェッチ結果を、適宜ラベル達に書き出し
	static NSDateFormatter *dateFormatter = nil;
	if (dateFormatter == nil) {
		dateFormatter = [[NSDateFormatter alloc] init];
//		[dateFormatter setLocale:[[[NSLocale alloc] initWithLocaleIdentifier:@"sysytemLocale"] autorelease]];
//		[dateFormatter setLocale:[[[NSLocale alloc] initWithLocaleIdentifier:@"jp_JP"] autorelease]];
//		[dateFormatter setLocale:[[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"] autorelease]];
//		[dateFormatter setLocale:[[[NSLocale alloc] initWithLocaleIdentifier:@"fr_FR"] autorelease]];
//		dateFormatter.dateFormat  = @"YYYY/MM/dd HH:mm:ss";	//書式設定
		[dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
		[dateFormatter setDateStyle:NSDateFormatterMediumStyle];
		[dateFormatter setDefaultDate:[NSDate date]];
	}
	NSString *timeString;
	NSString *digitString;
	NSString *dateString;
	
	UILabel *label;
	
	UIColor *hardColor   = [UIColor colorWithRed:1   green:0    blue:0.5 alpha:1];
//	UIColor *normalColor = [UIColor colorWithRed:0   green:0.25 blue:0.5 alpha:1];
	UIColor *normalColor = [UIColor colorWithRed:0.4 green:0.8  blue:1   alpha:1];
//	UIColor *easyColor   = [UIColor colorWithRed:0   green:0.5  blue:0   alpha:1];
	UIColor *easyColor   = [UIColor colorWithRed:0.4 green:1    blue:0.4 alpha:1];
	UIColor *helpLevelColor;
	
	for (int i = 0; i < [eventsArray count] && i < RECORD_MAX; i++){ 
		Entity *entity = (Entity *)[eventsArray objectAtIndex:i];

		switch ([[entity helpLevel] intValue]) {
			case HARD:
				helpLevelColor = hardColor;
				break;
			case NORMAL:
				helpLevelColor = normalColor;
				break;
			case EASY:
				helpLevelColor = easyColor;
				break;
			default:
				helpLevelColor = [UIColor whiteColor];				
				break;
		}
		
		//時間
		timeString = [NSString stringWithFormat:@"%06.3f",[[entity clearTime] doubleValue]];
		label = [labels objectAtIndex:tagToIndex[(i+1)*10 + 1]];
		label.textColor = helpLevelColor;
		label.text = timeString;
		label.font = [UIFont systemFontOfSize:18];
		
		//桁数
		digitString = [NSString stringWithFormat:@"%1d",[[entity digitOfNumbers] intValue]];
		label = [labels objectAtIndex:tagToIndex[(i+1)*10 + 2]];
		label.textColor = helpLevelColor;
		label.text = digitString;
		label.font = [UIFont fontWithName:@"courier" size:16];

		//日付
		dateString = [NSString stringWithFormat:@"%@",
					  [dateFormatter stringFromDate:[entity clearDate]]];
		label = [labels objectAtIndex:tagToIndex[(i+1)*10 + 3]];
		label.textColor = helpLevelColor;
		//24時間以内なら、New表示
		NSDate *nowTime = [NSDate date];	
		NSTimeInterval  since = [nowTime timeIntervalSinceDate:[entity clearDate]];
		if (since < 60 * 60 * 24){
			label.text = [dateString stringByAppendingString:NSLocalizedString(@"New", nil)];
		} else {
			label.text = [dateString stringByAppendingString:NSLocalizedString(@"Old", nil)];
		}
		label.font = [UIFont fontWithName:@"courier" size:12];

	}
	
}

- (IBAction)kindSelect:(id)sender {
	[self setNoRecordToLabel];
	[self displayRecord];
}

- (void)viewDidUnload
{
	//アプリケーションデリゲート
//	AppDelegate *appDelegate;
	
	//IB Outlet
	[self setKindSegmentedControl:nil];
	[self setLabels:nil];

	//CoreData用
//	NSManagedObjectContext 	*managedObjectContext;
//	NSMutableArray 			*eventsArray;

    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc
{
	//アプリケーションデリゲート
//	AppDelegate *appDelegate;
	
	//IB Outlet
	[kindSegmentedControl release];
	[labels release];

	//CoreData用
	//	[managedObjectContext release];
	//	[eventsArray release];

	[super dealloc];


}


@end
