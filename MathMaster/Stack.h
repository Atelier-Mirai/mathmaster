//
//  Stack.h
//  Mental ArithmeticKind Master
//
//  Created by 廣瀬 誠 on 平成23/11/17.
//  Copyright 23 岐阜IT協同組合. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSMutableArray (Stack)

- (id) pop;
- (id) peek;
- (void) push:(id)obj;


- (int) length;

@end

