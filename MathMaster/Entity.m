//
//  Entity.m
//  MathMaster
//
//  Created by 廣瀬 誠 on 平成23/11/25.
//  Copyright (c) 23 岐阜IT協同組合. All rights reserved.
//

#import "Entity.h"


@implementation Entity
@dynamic digitOfNumbers;
@dynamic arithmeticKind;
@dynamic clearTime;
@dynamic clearDate;
@dynamic helpLevel;

@end
