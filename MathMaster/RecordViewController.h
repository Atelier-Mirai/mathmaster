//
//  RecordViewController.h
//  Mental ArithmeticKind Master
//
//  Created by 廣瀬 誠 on 平成23/11/10.
//  Copyright 23 岐阜IT協同組合. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <UIKit/UIKit.h>
#import "Entity.h"
#import "AppDelegate.h"
#import "Constants.h"

@protocol RecordViewControllerDelegate;

@interface RecordViewController : UIViewController 
<UITextFieldDelegate,
UIAlertViewDelegate>
{
	
	//アプリケーションデリゲート
	AppDelegate *appDelegate;

	//IB Outlet
	UISegmentedControl 		*kindSegmentedControl;	//演算子選択用
	NSMutableArray 			*labels;				//記録表示用のラベル群
	
	//CoreData用
	NSManagedObjectContext 	*managedObjectContext;
	NSMutableArray 			*eventsArray;
	
}

//デリゲート
@property (retain, nonatomic) AppDelegate *appDelegate;


//IB Outlet
@property (nonatomic, retain) IBOutlet UISegmentedControl *kindSegmentedControl;
@property (nonatomic, retain) IBOutletCollection(UILabel) NSMutableArray *labels;

//CoreData用
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain) NSMutableArray *eventsArray;


@property (nonatomic, assign) id <RecordViewControllerDelegate> delegate;


//アクション
- (IBAction)back:(id)sender;		//戻る用
- (IBAction)reset:(id)sender;		//リセット用
- (IBAction)kindSelect:(id)sender;	//演算し選択用


//演算子の種類kindに応じた記録を表示する
- (void) displayRecord;
- (void) setNoRecordToLabel;




@end


@protocol RecordViewControllerDelegate
- (void)recordViewControllerDidFinish:(RecordViewController *)controller;
@end
