//
//  SettingViewController.h
//  Mental ArithmeticKind Master
//
//  Created by 廣瀬 誠 on 平成23/11/17.
//  Copyright 23 岐阜IT協同組合. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"


@protocol SettingViewControllerDelegate;

@interface SettingViewController : UIViewController {
	//設定値を保持する変数
	ArithmeticKind 		kind;			//演算の種類
	int 				digit;			//演算の桁数
	HelpLevel			helpLevel;		//ヒントのレベル(HARD/NORMAL/EASY)
    BOOL				bgm;			//BackGroundMusic の有無
	BOOL				se;				//SoundEffectの有無
	
	//設定変更したか否か
	BOOL				settingChanged;
	
	//IB Outlet	
	UISegmentedControl 	*kindSegmentedControl;	//演算を選択するためのセグメントコントロール
	UISegmentedControl 	*digitSegmentedControl;	//桁数を選択するためのセグメントコントロール

	UIButton 			*hardButton;			//ヒントレベル　
	UIButton 			*normalButton;
	UIButton 			*easyButton;
	
	UISwitch 			*bgmSwitch;				//BackGroundMusic の有無
	UISwitch 			*seSwitch;				//SoundEffectの有無
	
}

//設定値を保持する変数
@property (assign) ArithmeticKind 	kind;
@property (assign) int 			  	digit;
@property (assign) HelpLevel 	  	helpLevel;
@property (assign) BOOL				bgm;
@property (assign) BOOL				se;

//設定変更したか否か
@property (assign) BOOL				settingChanged;

//IB Outlet	
@property (nonatomic, retain) IBOutlet UISegmentedControl *kindSegmentedControl;
@property (nonatomic, retain) IBOutlet UISegmentedControl *digitSegmentedControl;

@property (nonatomic, retain) IBOutlet UIButton *hardButton;
@property (nonatomic, retain) IBOutlet UIButton *normalButton;
@property (nonatomic, retain) IBOutlet UIButton *easyButton;

@property (nonatomic, retain) IBOutlet UISwitch *bgmSwitch;
@property (nonatomic, retain) IBOutlet UISwitch *seSwitch;


//アクション
- (IBAction)selectArithmeticKind:(id)sender;
- (IBAction)selectDigitOfNumbers:(id)sender;

- (IBAction)helpLevelSetting:(id)sender;

- (IBAction)bgmSwitchAction:(id)sender;
- (IBAction)seSwitchAction:(id)sender;


- (IBAction)back:(id)sender;			//戻るボタン




@property (nonatomic, assign) id <SettingViewControllerDelegate> delegate;

@end


/* delegate */
@protocol SettingViewControllerDelegate
- (void)settingViewControllerDidFinish:(SettingViewController *)controller settingChanged:(BOOL)settingChanged;

@end

