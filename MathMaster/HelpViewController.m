//
//  HelpViewController.m
//  Mental ArithmeticKind Master
//
//  Created by 廣瀬 誠 on 平成23/11/10.
//  Copyright 23 岐阜IT協同組合. All rights reserved.
//

#import "HelpViewController.h"

#import <QuartzCore/QuartzCore.h>		//ボタンの角を丸くするために



@implementation HelpViewController

@synthesize helpButtonNounProject = _helpButtonNounProject;
@synthesize helpButtonBeizGraphics = _helpButtonBeizGraphics;
@synthesize helpButtonYodoyabashi = _helpButtonYodoyabashi;
@synthesize helpButtonTabiPhoto = _helpButtonTabiPhoto;
@synthesize scrollView = _scrollView;
@synthesize delegate=_delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
	[_helpButtonNounProject release];
	[_helpButtonBeizGraphics release];
	[_helpButtonYodoyabashi release];
	[_helpButtonTabiPhoto release];
    [_scrollView release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	_helpButtonNounProject.layer.masksToBounds = YES;
	_helpButtonNounProject.layer.cornerRadius = 10;
	_helpButtonBeizGraphics.layer.masksToBounds = YES;
	_helpButtonBeizGraphics.layer.cornerRadius = 10;
	_helpButtonYodoyabashi.layer.masksToBounds = YES;
	_helpButtonYodoyabashi.layer.cornerRadius = 10;
	_helpButtonTabiPhoto.layer.masksToBounds = YES;
	_helpButtonTabiPhoto.layer.cornerRadius = 10;
	
	_scrollView.contentSize = CGSizeMake(320, 436+1);

	
}

- (void)viewDidUnload
{
	[self setHelpButtonNounProject:nil];
	[self setHelpButtonBeizGraphics:nil];
	[self setHelpButtonYodoyabashi:nil];
	[self setHelpButtonTabiPhoto:nil];
    [self setScrollView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)back:(id)sender {
	
	
	
	[self.delegate helpViewControllerDidFinish:self];
}

- (IBAction)buttonNounProject:(id)sender {
	NSURL *url = [NSURL URLWithString:@"http://thenounproject.com/"];
  	UIApplication *app = [UIApplication sharedApplication];
    [app openURL:url];
}

- (IBAction)buttonBeizGraphics:(id)sender {
	NSURL *url = [NSURL URLWithString:@"http://www.beiz.jp/"];
  	UIApplication *app = [UIApplication sharedApplication];
    [app openURL:url];
}

- (IBAction)buttonYodoyabashi:(id)sender {
	NSURL *url = [NSURL URLWithString:@"http://www.yodoyabashift.com/"];
  	UIApplication *app = [UIApplication sharedApplication];
    [app openURL:url];
}

- (IBAction)buttonTabiPhoto:(id)sender {
	NSURL *url = [NSURL URLWithString:@"http://photo.yu-travel.net/"];
  	UIApplication *app = [UIApplication sharedApplication];
    [app openURL:url];
}
@end
