//
//  MainViewController.m
//  Mental ArithmeticKind Master
//
//  Created by 廣瀬 誠 on 平成23/11/10.
//  Copyright 23 岐阜IT協同組合. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>		//ボタンの角を丸くするために
#import <AVFoundation/AVFoundation.h>	//BGM再生のために
#import <AudioToolbox/AudioServices.h>	//タップ音を出すために

#import "MainViewController.h"
#import "Stack.h"
#import "Entity.h"
#import "Constants.h"


#pragma mark - BGM ListenerCallBack
//ヘッドフォンが引き抜かれたときに、一時停止する。
void audioRouteChangeListenerCallback (void *inUserData, AudioSessionPropertyID inPropertyID, UInt32 inPropertyValueSize, const void *inPropertyValue ) {
	
	if (inPropertyID != kAudioSessionProperty_AudioRouteChange) {
		return;	
	}
	MainViewController *controller = (MainViewController *) inUserData;
	
	if (controller.bgmPlayer.playing == 0 ) {
//		NSLog(@"オーディオが再生されていないときにヘッドフォンが取り外された/取り付けられた");
		return;
	} else {
		//ルート変更の理由を確認する。
		CFDictionaryRef	routeChangeDictionary = inPropertyValue;
		CFNumberRef routeChangeReasonRef = CFDictionaryGetValue (routeChangeDictionary, CFSTR(kAudioSession_AudioRouteChangeKey_Reason) );
		SInt32 routeChangeReason;
		CFNumberGetValue (routeChangeReasonRef, kCFNumberSInt32Type, &routeChangeReason);
		if (routeChangeReason == kAudioSessionRouteChangeReason_OldDeviceUnavailable) {
			[controller.bgmPlayer pause];
//			NSLog (@"ヘッドフォンが外されたので、一時停止する");
		} else {
//			NSLog (@"本体で再生中に、ヘッドフォンを繋いだので、ヘッドフォンから出力する");
		}
	}
}

#pragma mark - @implementation

@implementation MainViewController

//アプリケーションデリゲート
@synthesize applicationDelegate;

//プログラム内で使用するメンバ変数
@synthesize startTime;
@synthesize remainingTime;
@synthesize questionNumber;
@synthesize answerNumber;
@synthesize chipArray;
@synthesize needsChipList;	//お題を解く上でどのチップが必要か格納する配列
@synthesize chipStack;		//押されたチップを格納するスタック

//設定のための変数
@synthesize arithmeticKind;
@synthesize digitOfNumbers;
@synthesize	helpLevel;
@synthesize	backGroundMusic;
@synthesize	soundEffect;

//@synthesize settingChanged;

//BGM・SE関係
@synthesize bgmURL;
@synthesize bgmPlayer;

@synthesize tapURL;
@synthesize tapPlayer;
@synthesize fanfareURL;
@synthesize fanfarePlayer;
@synthesize consolationURL;
@synthesize consolationPlayer;

@synthesize playing;
@synthesize interruptedOnPlayback;

//タップしたときに音を出させないようにする。（初期配置でチップを並べるとき）
@synthesize isValidSE;

//タイマー用
//NSTimerにかんしては不要
@synthesize countDownNumber;

//Core Data 用
@synthesize managedObjectContext = _managedObjectContext;
@synthesize eventsArray;

//IBが生成するメンバ変数　Outlet
//ディスプレイエリア　（画面上部1/3)
@synthesize questionTitleLabel;
@synthesize answerTitleLabel;
@synthesize timeDisplayLabel;
@synthesize timeProgressBar;
@synthesize questionLabel;
@synthesize userInputDataDisplayLabel;
@synthesize operand1Label;
@synthesize operand2Label;
@synthesize displayAreaColorView;
@synthesize currentAnswer;
@synthesize speechBalloonView;
@synthesize speechBalloonLabel;
@synthesize speechBalloonImageView;

//達成画面（ビュー）
@synthesize congratulationView;
@synthesize congratulationImageView;
@synthesize messageLabel;
@synthesize phoneticLabel;
@synthesize newRecordLabel;
@synthesize recordTimeLabel;
@synthesize closeLabel;

//チップ（ボタン）群をのせるためのビュー
@synthesize chipBackgroundView;

//mask用
@synthesize maskView;
@synthesize countDownLabel;
@synthesize maskViewForAll;

//ガイダンス
@synthesize guidanceView;

@synthesize SBPlayView;
@synthesize SBPlayImageView;
@synthesize SBPlayLabel;
@synthesize imageView;

@synthesize SBRecordView;
@synthesize SBRecordImageView;
@synthesize SBRecordLabel;

@synthesize SBHelpView;
@synthesize SBHelpImageView;
@synthesize SBHelpLabel;

@synthesize SBSettingsView;
@synthesize SBSettingsImageView;
@synthesize SBSettingsLabel;

@synthesize guidanceTextView;

@synthesize guidanceFlag;
@synthesize openingImmediatelyLaterFlag;
@synthesize guidanceCloseButton;

//複数演算子確認用
@synthesize pluralOperator;


#pragma mark - BGM
- (void) setupApplicationBGM {
	
	//リソースから音源を生成する
	NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"BGM" ofType:@"aif"];
	bgmURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];

	soundFilePath = [[NSBundle mainBundle] pathForResource:@"tapSE" ofType:@"aif"];
	tapURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
	soundFilePath = [[NSBundle mainBundle] pathForResource:@"fanfareSE" ofType:@"aif"];
	fanfareURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
	soundFilePath = [[NSBundle mainBundle] pathForResource:@"consolationSE" ofType:@"aif"];
	consolationURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
	
	// オーディオセッションデリゲートの設定
	[[AVAudioSession sharedInstance] setDelegate: self];
	
	//背景で音楽が再生されていたら、両方再生するようカテゴリを設定
	[[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryAmbient error: nil];
	
	//iPodで音楽を再生中にiPodの音量を小さくする
	UInt32 small;
	AudioSessionSetProperty(kAudioSessionProperty_OtherMixableAudioShouldDuck,sizeof(small), &small);
	
	// コールバック関数の登録
	// ヘッドフォンが抜かれたときに、audioRouteChangeListenerCallback関数が呼ばれるようになる。
	AudioSessionAddPropertyListener (
									 kAudioSessionProperty_AudioRouteChange,
									 audioRouteChangeListenerCallback,
									 self
									 );
	
	// オーディオセッションのアクティベート
	NSError *activationError = nil;
	[[AVAudioSession sharedInstance] setActive: YES error: &activationError];
	
	// オーディオプレーヤーインスタンスの生成
	bgmPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL: bgmURL error: nil];
	tapPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL: tapURL error: nil];
	fanfarePlayer = [[AVAudioPlayer alloc] initWithContentsOfURL: fanfareURL error: nil];
	consolationPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL: consolationURL error: nil];

	
	
	
	//すぐに再生できるように、準備しておく
	[bgmPlayer prepareToPlay];
	[bgmPlayer setVolume: 0.5];
	[bgmPlayer setDelegate: self];
	
	[tapPlayer prepareToPlay];
	[tapPlayer setVolume: 1.0];
	[tapPlayer setDelegate: self];
	[fanfarePlayer prepareToPlay];
	[fanfarePlayer setVolume: 1.0];
	[fanfarePlayer setDelegate: self];
	[consolationPlayer prepareToPlay];
	[consolationPlayer setVolume: 1.0];
	[consolationPlayer setDelegate: self];
	
}



#pragma mark AV Foundation delegate methods
//割り込み処理（途中で電話がかかってきたときの処理）
- (void) audioPlayerDidFinishPlaying: (AVAudioPlayer *) appSoundPlayer
						successfully: (BOOL) flag {
	playing = NO;
}

//割り込まれた時に、一時停止する。
- (void) audioPlayerBeginInterruption: player {
	if (playing) {
		playing = NO;
		interruptedOnPlayback = YES;
	}
}

//割り込みが終わったときに、再生を再開する
- (void) audioPlayerEndInterruption: player {
	//オーディオセッションを再アクティベーション
	[[AVAudioSession sharedInstance] setActive: YES error: nil];
	
	if (interruptedOnPlayback) {
		[bgmPlayer prepareToPlay];
		[bgmPlayer play];
		playing = YES;
		interruptedOnPlayback = NO;
	}
}


- (void) tapSE_Play{
	if (isValidSE && soundEffect){
		tapPlayer.numberOfLoops = 0;
		tapPlayer.currentTime = 0;
		[tapPlayer play];
		tapPlayer.currentTime = 0;
	}
}

#pragma mark - userDefault

//デフォルト値設定
+ (void)initialize {
	NSMutableDictionary *defaultValues = [NSMutableDictionary dictionaryWithCapacity:2];
	[defaultValues setValue:ADDITION forKey:@"ARITHMETIC_KIND"];	//足し算
	[defaultValues setValue:@"2" forKey:@"DIGIT_OF_NUMBERS"];		//二桁
	[defaultValues setValue:@"0" forKey:@"HELP_LEVEL"];				//EASY
	[defaultValues setValue:@"1" forKey:@"BGM"];					//BGM=ON
	[defaultValues setValue:@"1" forKey:@"SE"];						//SE=ON
	[defaultValues setValue:@"1" forKey:@"GUIDANCE"];				//ガイダンス有り
	
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	[userDefaults registerDefaults:defaultValues];
}

- (void)saveUserDefault {
	//演算の種類、使用する数の桁数をユーザーデフォルトに書き込む
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	[userDefaults setInteger:arithmeticKind forKey:@"ARITHMETIC_KIND"];
	[userDefaults setInteger:digitOfNumbers forKey:@"DIGIT_OF_NUMBERS"];
	[userDefaults setInteger:helpLevel forKey:@"HELP_LEVEL"];
	[userDefaults setBool:backGroundMusic forKey:@"BGM"];
	[userDefaults setBool:soundEffect forKey:@"SE"];
	[userDefaults setBool:guidanceFlag forKey:@"GUIDANCE"];
	[userDefaults synchronize];
}

- (void)loadUserDefault {
	//演算の種類、使用する数の桁数をユーザーデフォルトから読み込む
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	arithmeticKind = [userDefaults integerForKey:@"ARITHMETIC_KIND"];
	digitOfNumbers = [userDefaults integerForKey:@"DIGIT_OF_NUMBERS"];
	helpLevel = [userDefaults integerForKey:@"HELP_LEVEL"];
	backGroundMusic = [userDefaults boolForKey:@"BGM"];
	soundEffect = [userDefaults boolForKey:@"SE"];
	guidanceFlag = [userDefaults boolForKey:@"GUIDANCE"];
}

- (void) showUserDefault:(NSString*)str {
//	NSLog(@"<< %@ >> kind:%d, digit:%d, helpLevel:%d, bgm:%d, se:%d guidanceFlag:%d", str, arithmeticKind, digitOfNumbers, helpLevel, backGroundMusic, soundEffect, guidanceFlag);
}

#pragma mark - timer

//タイマーの開始処理
- (void)displayTimerStart {
	
	startTime = [[NSDate date]retain];
	
	if (displayTimer == nil) {
        //タイマーの生成
//		NSLog(@"generate new displayTimer");
        displayTimer = [NSTimer scheduledTimerWithTimeInterval:(double)1/(double)117
														target:self
													  selector:@selector(displayTimeSet)
													  userInfo:nil
													   repeats:YES];	//retain不要
    } else {
//		NSLog(@"displayTimer not nil");
	}
	
}

//タイマーの停止処理
- (void)displayTimerStop {
	
	if (displayTimer != nil){
		//タイマーの停止
		[displayTimer invalidate];
		displayTimer = nil;	//release不要
//		NSLog(@"displayTimer stop");
	} else {
//		NSLog(@"displayTimer else");
	}
	
}

//タイマーの開始処理
- (void)decisionTimerStart {
	
	startTime = [[NSDate date]retain];
	
	if (decisionTimer == nil) {
        //タイマーの生成
//		NSLog(@"generate new decisionTimer");
        decisionTimer = [NSTimer scheduledTimerWithTimeInterval:(double)1/(double)4
														 target:self
													   selector:@selector(timeCheck:)
													   userInfo:nil
														repeats:YES];	//retain不要
    } else {
//		NSLog(@"decisionTimer not nil");
	}
	
}

//タイマーの停止処理
- (void)decisionTimerStop {
	
	if (decisionTimer != nil){
		//タイマーの停止
		[decisionTimer invalidate];
		decisionTimer = nil;	//release不要
//		NSLog(@"decisionTimer stop");
	} else {
//		NSLog(@"decisionTimer else");
	}
	
}

//タイマーの開始処理
- (void)countDownTimerStart {
	
	if (countDownTimer == nil) {
        //タイマーの生成
//		NSLog(@"generate new countDownTimer");
        countDownTimer = [NSTimer scheduledTimerWithTimeInterval:(double)1/(double)1
														 target:self
													   selector:@selector(showCountDown)
													   userInfo:nil
														repeats:YES];	//retain不要
    } else {
//		NSLog(@"countDonwTimer not nil");
	}
	
}

//タイマーの停止処理
- (void)countDownTimerStop {
	
	if (countDownTimer != nil){
		//タイマーの停止
		[countDownTimer invalidate];
		countDownTimer = nil;	//release不要
//		NSLog(@"countDownTimer stop");
	} else {
//		NSLog(@"countDownTimer else");
	}
	
}

#pragma mark - guidance

//タイマーの開始処理
- (void)guidanceTimerStart {
	
	if (guidanceTimer == nil) {
        //タイマーの生成
//		NSLog(@"generate new guidanceTimer");
        guidanceTimer = [NSTimer scheduledTimerWithTimeInterval:(double)1/(double)30
														 target:self
													   selector:@selector(aminationStart)
													   userInfo:nil
														repeats:YES];	//retain不要
    } else {
		//		NSLog(@"guidanceTimer not nil");
	}
	
}

//タイマーの停止処理
- (void)guidanceTimerStop {
	
	if (guidanceTimer != nil){
		//タイマーの停止
		[guidanceTimer invalidate];
		guidanceTimer = nil;	//release不要
//		NSLog(@"guidanceTimer stop");
	} else {
//		NSLog(@"guidanceTimer else");
	}
	
}


- (void) aminationStart {
	static int direction = 1;
	
	CGRect PlayFrame = SBPlayView.frame;
	int PlayY = PlayFrame.origin.y;
	
	if (PlayY < 0 || PlayY > 115-55){
		direction *= -1;
	}
	
	PlayFrame.origin.y = PlayY + direction * 2;
	SBPlayView.frame = PlayFrame;
	
	CGRect RecordFrame = SBRecordView.frame;
	int RecordY = RecordFrame.origin.y;
	
	RecordFrame.origin.y = RecordY - direction * 2;
	SBRecordView.frame = RecordFrame;
	
	CGRect HelpFrame = SBHelpView.frame;
	int HelpY = HelpFrame.origin.y;
	
	HelpFrame.origin.y = HelpY + direction * 2;
	SBHelpView.frame = HelpFrame;
	
	CGRect SettingsFrame = SBSettingsView.frame;
	int SettingsY = SettingsFrame.origin.y;
	
	SettingsFrame.origin.y = SettingsY - direction * 2;
	SBSettingsView.frame = SettingsFrame;
	
	
}

- (IBAction)guidanceStart:(id)sender {
	UIViewAnimationOptions options = UIViewAnimationOptionTransitionFlipFromRight;	//未使用
	
	//ローカライズ
	//英語はnibで記述
	if (applicationDelegate.isJapanease){
		SBPlayLabel.text = @"おすと\nスタート";
		SBPlayLabel.numberOfLines = 0;
		
		SBRecordLabel.text = @"きろく\nだよ";
		SBRecordLabel.numberOfLines = 0;

		SBHelpLabel.text = @"あそび\nかただよ";
		SBHelpLabel.numberOfLines = 0;

		SBSettingsLabel.text = @"せってい\nだよ";
		SBSettingsLabel.numberOfLines = 0;
		
		guidanceTextView.text = @"がめんのまん中に\nおだいがでるよ\nけいさんして\nおだいのかずになるように\nボタンをおしてね\n\nこのがめんはさいしょだけでるよ\nボタンをおすときえるよ";
		
		[guidanceCloseButton setTitle:@"とじる" forState:UIControlStateNormal];

		
	}
	
	
	SBPlayImageView.frame = CGRectMake(0, 0, 79/10, 55/10);
	SBPlayImageView.center = CGPointMake(79/10/2, 55/10/2);
	SBPlayLabel.hidden = YES;
	
	SBRecordImageView.frame = CGRectMake(0, 0, 79/10, 55/10);
	SBRecordImageView.center = CGPointMake(79/10/2, 55/10/2);
	SBRecordLabel.hidden = YES;
	
	SBHelpImageView.frame = CGRectMake(0, 0, 79/10, 55/10);
	SBHelpImageView.center = CGPointMake(79/10/2, 55/10/2);
	SBHelpLabel.hidden = YES;
	
	SBSettingsImageView.frame = CGRectMake(0, 0, 79/10, 55/10);
	SBSettingsImageView.center = CGPointMake(79/10/2, 55/10/2);
	SBSettingsLabel.hidden = YES;
	
	
	
	//アニメーションの実行
	[UIView animateWithDuration:3.0
						  delay:0.0 
						options:options
					 animations:^{ 
						 SBPlayImageView.frame = CGRectMake(0, 0, 79, 55);
						 SBPlayImageView.center = CGPointMake(79/2, 55/2);
						 
						 SBRecordImageView.frame = CGRectMake(0, 0, 79, 55);
						 SBRecordImageView.center = CGPointMake(79/2, 55/2);
						 
						 SBHelpImageView.frame = CGRectMake(0, 0, 79, 55);
						 SBHelpImageView.center = CGPointMake(79/2, 55/2);
						 
						 SBSettingsImageView.frame = CGRectMake(0, 0, 79, 55);
						 SBSettingsImageView.center = CGPointMake(79/2, 55/2);
						 
						 
					 }
					 completion:^(BOOL finished) {
						 SBPlayLabel.hidden = NO;
						 SBPlayLabel.frame = CGRectMake(0, 7, 79, 55);
						 SBPlayLabel.center = CGPointMake(79/2, 7 + 55/2);
						 
						 SBRecordLabel.hidden = NO;
						 SBRecordLabel.frame = CGRectMake(0, 7, 79, 55);
						 SBRecordLabel.center = CGPointMake(79/2, 7 + 55/2);
						 
						 SBHelpLabel.hidden = NO;
						 SBHelpLabel.frame = CGRectMake(0, 7, 79, 55);
						 SBHelpLabel.center = CGPointMake(79/2, 7 + 55/2);
						 
						 SBSettingsLabel.hidden = NO;
						 SBSettingsLabel.frame = CGRectMake(0, 7, 79, 55);
						 SBSettingsLabel.center = CGPointMake(79/2, 7 + 55/2);
						 
						 //一秒待ってから
						 [UIView animateWithDuration:1 delay:0 options:0 animations:nil completion:nil];
						 //上下に動くアニメーション開始する
						 [self guidanceTimerStart];
						 
						 
					 }];
	
	
	
}
- (IBAction)guidanceClose:(id)sender {

	guidanceFlag = NO;
	//次回以降、ガイダンスを行わないように、
	//ユーザーデフォルトに書き込み
	[self saveUserDefault];
	
//	NSLog(@"guidanceClose");
	guidanceView.hidden = YES;
	maskViewForAll.hidden = YES;
	
}



#pragma mark - Play CountDown


//カウントダウン表示用メソッド
- (void) showCountDown {

	//カウントダウン中はBGM止める
	[bgmPlayer stop];
	playing = NO;

	//画面暗くする
	[self maskViewSetForCountDown:YES];

	//カウントダウン
	countDownNumber--;
	
	if (countDownNumber < 0){
		//タイマーストップ
		[self countDownTimerStop];
		[self maskViewSetForCountDown:NO];
		
		//カウントダウン終了したのでお題が見えるように、元の色にしておく
		questionLabel.textColor = [UIColor whiteColor];
		currentAnswer.textColor = [UIColor blackColor];
		userInputDataDisplayLabel.textColor = [UIColor blackColor];
		
		//プレイ用にタイマースタート
		[self displayTimerStart];
		[self decisionTimerStart];

		if (backGroundMusic){
			//もしBGM有効になっていたら、BGM再生
			bgmPlayer.numberOfLoops = -1;	//無限に繰り返す
			bgmPlayer.currentTime = 0;		//最初から
			[bgmPlayer play];
			playing = YES;
		}
	}
}

- (void) maskViewSetForUnTouched:(BOOL)yesOrNo {
	
	switch (yesOrNo) {
			//マスクをかける
		case YES:
			maskView.hidden = NO;
			maskView.backgroundColor = [UIColor blackColor];
			maskView.alpha = 0.4;
			
			countDownLabel.hidden = YES;
			break;
			
			//マスクをかけない
		case NO:
			maskView.hidden = YES;
			countDownLabel.hidden = YES;
			break;
	}
	
}

- (void) maskViewSetForCountDown:(BOOL)yesOrNo {
	switch (yesOrNo) {
			//マスクをかける
		case YES:
			maskView.hidden = YES;
			maskView.backgroundColor = [UIColor blackColor];
			maskView.alpha = 0.0;
			maskViewForAll.hidden = NO;
			maskViewForAll.backgroundColor = [UIColor blackColor];
			maskViewForAll.alpha = 0.8;
			
			
			countDownLabel.hidden = NO;
			countDownLabel.alpha = 1;
			countDownLabel.text = [NSString stringWithFormat:@"%d", countDownNumber];
			
			break;
			
			//マスクをかけない
		case NO:
			maskView.hidden = YES;
			maskViewForAll.hidden = YES;
			countDownLabel.hidden = YES;
			break;
	}
}

#pragma mark - Play makeQuestion


//ArithmeticKind -> operatorString;
NSString *kindToString (ArithmeticKind arithmeticKind){
	NSString *operatorString;
	switch (arithmeticKind) {
		case ADDITION:		operatorString = @"+";	break;
		case SUBTRACTION:	operatorString = @"-";	break;
		case MULTIPLICATION:operatorString = @"×";	break;
		case DIVISION:		operatorString = @"÷";	break;
	}
	return operatorString;
}

//桁数を与えると、その桁数の乱数を返す関数。
int randomNumberSpecifiedDigit (int digit){
	int randomNumber;
	srand(time(NULL));
	do {
		randomNumber = rand() % (int)powf(10.0f, (float)digit);
	} while (randomNumber < (int)powf(10.0f, (float)digit-1) );
	return randomNumber;
}

//どのチップが配置済みか確認するために
//chip...Array[30] #=> 10 なら、マップの３０番目の場所には１０と表示されている。
int chipUseSituationRegistrationArray[49];


//ボタンを作るメソッド
- (UIButton*) makeButton:(int)location text:(NSString *)text {

	//ボタンタイプの指定
	UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
	//	UIButton *button = [UIButton buttonWithType:111];

	//ボタンの位置・サイズ設定
	CGRect rect = CGRectMake(/*2+*/CHIP_SIZE*(location%MAP_SIZE), /*44+115+2+*/CHIP_SIZE*(location/MAP_SIZE), CHIP_SIZE, CHIP_SIZE);
	button.frame = rect;
	
	//タグの設定
	button.tag = location;
	//ボタンのフォント設定
	[button.titleLabel setFont:[UIFont systemFontOfSize:36] ];
	//文字設定
	[button setTitle:text forState:UIControlStateNormal];
	 
	//演算子に応じたボタンの色設定
	switch (arithmeticKind) {
		case ADDITION:
			//ボタンの色
			[button setBackgroundImage:[UIImage imageNamed:@"imageAdd.png"] forState:UIControlStateNormal];
			//文字色設定
			if ([text isEqualToString:kindToString(arithmeticKind)]){
				[button setTitleColor:[UIColor colorWithRed:1 green:0 blue:0.5 alpha:1] forState:UIControlStateNormal];
			} else {
				[button setTitleColor:[UIColor colorWithRed:0 green:0.5 blue:1 alpha:1] forState:UIControlStateNormal];
			}
			break;

		case SUBTRACTION:
			//ボタンの色
			[button setBackgroundImage:[UIImage imageNamed:@"imageSub.png"] forState:UIControlStateNormal];
			//文字色設定
			if ([text isEqualToString:kindToString(arithmeticKind)]){
				[button setTitleColor:[UIColor colorWithRed:0 green:0.5 blue:1 alpha:1] forState:UIControlStateNormal];
			} else {
				[button setTitleColor:[UIColor colorWithRed:1 green:0 blue:0.5 alpha:1] forState:UIControlStateNormal];
			}
			break;
		
		case MULTIPLICATION:
			//ボタンの色
			[button setBackgroundImage:[UIImage imageNamed:@"imageMul.png"] forState:UIControlStateNormal];
			//文字色設定
			if ([text isEqualToString:kindToString(arithmeticKind)]){
				[button setTitleColor:[UIColor colorWithRed:1 green:0 blue:0.5 alpha:1] forState:UIControlStateNormal];
			} else {
				[button setTitleColor:[UIColor colorWithRed:0 green:0.5 blue:1 alpha:1] forState:UIControlStateNormal];
			}
			break;

		case DIVISION:
			//ボタンの色
			[button setBackgroundImage:[UIImage imageNamed:@"imageDiv.png"] forState:UIControlStateNormal];
			//文字色設定
			if ([text isEqualToString:kindToString(arithmeticKind)]){
				[button setTitleColor:[UIColor colorWithRed:0 green:0.5 blue:1 alpha:1] forState:UIControlStateNormal];
			} else {
				[button setTitleColor:[UIColor colorWithRed:1 green:0 blue:0.5 alpha:1] forState:UIControlStateNormal];
			}
			break;
	}

	//文字の影の色設定
	[button setTitleShadowColor:[UIColor blackColor] forState:UIControlStateNormal];
	[button.titleLabel setShadowOffset:CGSizeMake(1, 1)];

	//タップできるようにする
	button.userInteractionEnabled = YES;	//タップできるようにする。
											//デフォルトでもタップできるので不要だけれど、念のため
	//ボタンを押すと光るようにしない。
	button.reversesTitleShadowWhenHighlighted = NO;
	
	//ボタンが押されたときに呼ばれるメソッドの設定
	[button addTarget:self
			   action:@selector(chipTouched:)
	 forControlEvents:UIControlEventTouchDown];
	
	return button;
	
}

// お題の生成
- (void) makeQuestionAndOperands {
	
	do {
	
		NSInteger provisionalQuestionNumber = -1;	//仮のお題
		
		//　＃＃＃　各種演算にあわせた設定　＃＃＃
		int firstOperand;					//第一被演算子
		int secondOperand;				//第二被演算子
		switch (arithmeticKind) {
			case ADDITION:
				//乱数の種を初期化
				srand(time(NULL));
				//仮のお題に４桁（１０００〜９９９９）の乱数をセット
				provisionalQuestionNumber = randomNumberSpecifiedDigit(digitOfNumbers);
				//provisionalQuestionNumber == 1;だと無限ループになるので修正
				if (provisionalQuestionNumber == 1){
					provisionalQuestionNumber = (int)powf(10.0f, (float)digitOfNumbers) - 1;
				}
				do {
					firstOperand = rand() % provisionalQuestionNumber;
				} while (firstOperand == 0);
				secondOperand = provisionalQuestionNumber - firstOperand;
				break;
				
			case SUBTRACTION:
				srand(time(NULL)+1);
				provisionalQuestionNumber = randomNumberSpecifiedDigit(digitOfNumbers);
				int randomNumber;
				do {
					randomNumber = rand() % (int)powf(10.0f, (float)digitOfNumbers);
				} while (randomNumber < (int)powf(10.0f, (float)digitOfNumbers-1) );
				secondOperand = randomNumber;
				firstOperand = provisionalQuestionNumber + secondOperand;
				break;
				
			case MULTIPLICATION:
				if (digitOfNumbers == 1){
					do {
						srand(time(NULL)+2);
						provisionalQuestionNumber = randomNumberSpecifiedDigit(digitOfNumbers);
						firstOperand = ((int)sqrt(provisionalQuestionNumber)) + (rand() % (3<<digitOfNumbers));
						if (firstOperand == 0){
							continue;
						}
						secondOperand = provisionalQuestionNumber / firstOperand;
					} while (secondOperand == 0);
					provisionalQuestionNumber = firstOperand * secondOperand;
					break;
				} else {
					do {
						srand(time(NULL)+2);
						provisionalQuestionNumber = randomNumberSpecifiedDigit(digitOfNumbers);
						firstOperand = ((int)sqrt(provisionalQuestionNumber)) + (rand() % (3<<digitOfNumbers));
						if (firstOperand == 0){
							continue;
						}
						secondOperand = provisionalQuestionNumber / firstOperand;
					} while (secondOperand == 0 || secondOperand == 1);		//×1の回答を許さないために
					provisionalQuestionNumber = firstOperand * secondOperand;
					break;
					
				}
				
			case DIVISION:
				srand(time(NULL)+3);
				provisionalQuestionNumber = randomNumberSpecifiedDigit(digitOfNumbers);
				do {
					//平方根を目安に被演算子を作成
					if (digitOfNumbers == 1){
						randomNumber = rand() % 10 + 1;
					} else {
						randomNumber = (int)sqrt(rand() % (int)powf(10.0f, (float)digitOfNumbers));
					}
					//				NSLog(@"randomNumber:%d", randomNumber);
				} while (randomNumber == 0);

				//2桁以上のお題のとき、÷1の回答を許さないために
				if (digitOfNumbers == 1){
					secondOperand = randomNumber;
					firstOperand = provisionalQuestionNumber * secondOperand;
				} else {
					if (randomNumber == 1){
						secondOperand = randomNumber * 2;
						firstOperand = provisionalQuestionNumber * secondOperand;
					} else {
						secondOperand = randomNumber;
						firstOperand = provisionalQuestionNumber * secondOperand;
					}
				}
				break;
		}
		
		
		//タイムアップのときの答え表示用に、第一被演算子と第二被演算子を保存。
		operand1Label.hidden = YES;
		operand2Label.hidden = YES;
		operand1Label.text = [NSString stringWithFormat:@"%d", firstOperand];
		operand2Label.text = [NSString stringWithFormat:@"%d", secondOperand];
		
		//お題セット
		questionNumber = provisionalQuestionNumber;
		
		//万一、桁数が違っているようなら繰り返す
	} while ([[NSString stringWithFormat:@"%d", questionNumber] length] != digitOfNumbers); 
	
}

// お題とチップ配置を決定するメソッド
- (void) questionAndChipAssignmentOpening:(BOOL)opening{
	
	//もし吹き出しが見えているとかっこわるいので消す。
	speechBalloonView.hidden = YES;
	
	//初期化
	userInputDataDisplayLabel.text = @"";
	pluralOperator = NO;
	
	//初期化
	needsChipList = [[NSMutableArray alloc] initWithCapacity:1];

	//オープニングで配置された仮のチップや、前のプレイで配置されたチップを取り除く
	if (!opening){
		for (UIButton *button in chipArray){
			[button removeFromSuperview];
		}
	}
	
	//チップアレイの初期化
	chipArray = [[NSMutableArray alloc] initWithCapacity:CHIP_NUMBER];
	for (int i = 0; i < CHIP_NUMBER; i++){
		[chipArray addObject:@"VACANT"];
	}
	
	// お題と被演算子の生成
	[self makeQuestionAndOperands];

	//被演算子に含まれている「数字」を取得する。
	NSString *firstStr = operand1Label.text;
	NSString *secondStr = operand2Label.text;
	NSString *str;
	
	for (int i = 0; i < [firstStr length]; i++){
		//一文字ずつ取り出して、格納する
		str = [firstStr substringWithRange:NSMakeRange(i,1)];
		[needsChipList addObject:str];
	}
	//演算子を追加
	[needsChipList addObject:kindToString(arithmeticKind)];
	
	//第一被演算子と第二被演算子の間の区切り。
	int delimiter = [needsChipList count];
	
	for (int i = 0; i < [secondStr length]; i++){
		//一文字ずつ取り出して、格納する
		str = [secondStr substringWithRange:NSMakeRange(i,1)];
		[needsChipList addObject:str];
	}
	
	//確認
//	for (int i = 0; i < [needsChipList count]; i++){
//		NSLog(@"i:%d list:%@", i, [needsChipList objectAtIndex:i] );
//	}
	
	questionLabel.text = [[NSNumber numberWithInt:questionNumber] stringValue];
	
	//
	// チップ配置
	//
	//まずはneedsChipを配置してしまう。
	
	//どのチップが配置済みか確認するために
	//chip...Array[30] #=> 10 なら、マップの３０番目の場所には１０と表示されている。
	//	int chipUseSituationRegistrationArray[49];
	
	//初期化
	for (int i = 0; i < CHIP_NUMBER; i++){
		chipUseSituationRegistrationArray[i] = VACANT;
	}
	
	
	for (int i = 0; i < [needsChipList count]; i++){
		NSString *labelText = [needsChipList objectAtIndex:i];
		
		//ラベル生成
		int location;
		do {
			location = rand() % CHIP_NUMBER;	//使用済みでない乱数がでるまで回り続ける
		} while ( chipUseSituationRegistrationArray[location] != VACANT);
		UIButton *button = [self makeButton:location text:labelText];
		
		[self.chipBackgroundView addSubview:button];
		[button release];
		
		//チップ配列に格納 （選択したチップは二度使えないなど、あとの処理で使いたいので）
		[chipArray replaceObjectAtIndex:location withObject:button];
		//使用状況の更新
		if ([str isEqualToString:kindToString(arithmeticKind)]){
			chipUseSituationRegistrationArray[location] = 10;
		} else {
			chipUseSituationRegistrationArray[location] = [str intValue];
		}

		//オープニングでなければ、タッチしたことにする。
		if (!opening){		
			if (helpLevel == EASY && i < delimiter){
				[self chipTouched:button];
			}
		}
		
	}
	
	
	
	//これで、お題を解くために必要な数字と演算子の配置は終わったので、
	//残りのマスに演算子と適当に数字を配置する。
	for (int location = 0; location < CHIP_NUMBER; location++){
		if (chipUseSituationRegistrationArray[location] != VACANT){
			continue;
		}
		
		//ラベルの文字（数字＆演算子）を決める
		int labelNumber = rand() % 11;	//０〜９及び演算子で、チップは１１種類あるので
		NSString *labelText;
		if (labelNumber == 10){
			labelText = kindToString(arithmeticKind);
		} else {
			labelText = [NSString stringWithFormat:@"%d", labelNumber];
		}
		
		//ボタン生成
		UIButton *button = [self makeButton:location text:labelText];
		
		//作成したラベルをviewに追加する。（画面に表示する）
		[self.chipBackgroundView addSubview:button];
		[button release];
		
		//チップ配列に格納 （選択したチップは二度使えないなど、あとの処理で使いたいので）
		[chipArray replaceObjectAtIndex:location withObject:button];
		
		//使用状況の更新
		NSString *operatorString = kindToString(arithmeticKind);
		if ([labelText isEqualToString:operatorString]){
			chipUseSituationRegistrationArray[location] = 10;//演算子の意味で
		} else {
			chipUseSituationRegistrationArray[location] = [labelText intValue];
		}
	}
	
}


#pragma mark - Play Congratulation

Season isSeason(int month){
	switch (month){
		case 3:
		case 4:
		case 5:
			return SPRING;
		case 6:
		case 7:
		case 8:
			return SUMMER;
		case 9:
		case 10:
		case 11:
			return AUTUMN;
		case 12:
		case 1:
		case 2:
			return WINTER;
		case 0:
			return TIMEUP;		//TimeUp画面表示
		case -1:
			return OPENING;		//Opening画面表示
		default:				//ワーニング抑制のため
			return -999;
	}
}

//日本語環境に対応するよう、記述必要。
//端末の言語環境を取得し、コード記述する。

//達成画面を表示するメソッド
//時間と、もしランクインしていれば、何位にランクインしたか表示する。
//時間切れなら、残念画面を表示する

//オープニング画面も兼用
- (void) showCongratulationClearTime:(double)clearTime
								rank:(int)rank
							 opening:(BOOL)openingFlag
{
	
	//チップボタンに触れられないようにする。
	[self chipsUserInteractionEnabled:NO];
	
	//オープニングのとき
	if (openingFlag){
		if (backGroundMusic){
//			NSLog(@"showCon BGM play");
			//もしBGM有効になっていたら、BGM再生
			bgmPlayer.numberOfLoops = -1;	//無限に繰り返す
			[bgmPlayer play];
			playing = YES;
		}
	//クリアかタイムアップのとき
	} else {
		if (backGroundMusic){
			//鳴り続けているであろうBGMをとめる処理を記述。
//			NSLog(@"showCon BGM stop");
			[bgmPlayer stop];
			playing = NO;
		}


		if (soundEffect){
			//クリアのとき
			if (clearTime <= 60){
				//ファンファーレを鳴らす
//				NSLog(@"showCon fanfareSE");
				//AudioServicesPlaySystemSound(fanfareSE);
				fanfarePlayer.numberOfLoops = 0;
				fanfarePlayer.currentTime = 0;
				[fanfarePlayer play];
				fanfarePlayer.currentTime = 0;
			} else {
				//残念音を鳴らす
//				NSLog(@"showCon consolationSE");
				//AudioServicesPlaySystemSound(consolationSE);
				consolationPlayer.numberOfLoops = 0;
				consolationPlayer.currentTime = 0;
				[consolationPlayer play];
				consolationPlayer.currentTime = 0;
			}
		}
	}
	
	
	
	
	//季節に応じた背景画像をセットするために、月を取得
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	dateFormatter.dateFormat  = @"M";	//月を取得するよう設定
	int month = [[dateFormatter stringFromDate:[NSDate date]] intValue];
	[dateFormatter release];

	//達成画面　背景画像をセット
	if (clearTime > 60){
		month = 0;	//ゲームオーバー
	}
	if (openingFlag){
		month = -1;	//オープニング		
	}

	switch (isSeason(month)) {
		case OPENING:	
			congratulationImageView.image = [UIImage imageNamed:@"opening.png"];
			break;
		case TIMEUP:	//clearTime > 60 時間切れのとき
			congratulationImageView.image = [UIImage imageNamed:@"gameset.png"];
			break;
		case SPRING:
			congratulationImageView.image = [UIImage imageNamed:@"cherryblossom.png"];
			break;
		case SUMMER:
			congratulationImageView.image = [UIImage imageNamed:@"fireworks.png"];
			break;			
		case AUTUMN:
			congratulationImageView.image = [UIImage imageNamed:@"scarletleaves.png"];
			break;
		case WINTER:
			congratulationImageView.image = [UIImage imageNamed:@"snowfield.png"];
			break;
	}
	congratulationView.hidden = NO;

	//フォント定義
	UIFont *fontZipinfo = [UIFont fontWithName:@"Zapfino" size:24];
	UIFont *fontHiragino = [UIFont fontWithName:@"HiraKakuProN-W3" size:24];

	//おめでとうor残念ラベルの設定（文字列＆フォント）
	if (clearTime <= 60){
		messageLabel.text = NSLocalizedString(@"Congratulation!!", nil);
	} else {
		messageLabel.text = NSLocalizedString(@"Consolation prize.", nil);
	}
	messageLabel.font = fontZipinfo;

	//記録ラベルの設定（文字列＆フォント）

	NSString *str1, *str2;
	switch (applicationDelegate.isJapanease) {
		case YES:
			str1 = [NSString stringWithFormat:@"しんきろく!!\nだい %d い !!", rank];
			newRecordLabel.text = str1;
			[newRecordLabel setLineBreakMode:UILineBreakModeWordWrap];//改行モード
			[newRecordLabel setNumberOfLines:0];
			newRecordLabel.font = fontHiragino;
			break;
			
		case NO:
			str1 = [NSString stringWithFormat:@"New Record !!\nThe %d", rank];
			switch (rank){
				case 1:	str2 = @"st place.";	break;
				case 2:	str2 = @"nd place.";	break;
				case 3:	str2 = @"rd place.";	break;
				default: str2 = @"th place.";	break;
			};
			newRecordLabel.text = [str1 stringByAppendingString:str2];
			[newRecordLabel setLineBreakMode:UILineBreakModeWordWrap];//改行モード
			[newRecordLabel setNumberOfLines:0];
			newRecordLabel.font = fontHiragino;
			break;
	}
	

	
	//タイムラベルの設定（文字列＆フォント）
	
	switch (applicationDelegate.isJapanease) {
		case YES:
			recordTimeLabel.text = [NSString stringWithFormat:@"じかん %6.3f びょう", clearTime];
			recordTimeLabel.font = fontHiragino;
			recordTimeLabel.adjustsFontSizeToFitWidth = YES;
			break;
		case NO:
			recordTimeLabel.text = [NSString stringWithFormat:@"Time %6.3f sec", clearTime];
			recordTimeLabel.font = fontHiragino;
			recordTimeLabel.adjustsFontSizeToFitWidth = YES;
			break;
	}
	
	//閉じるラベルの設定（文字列＆フォント）
	closeLabel.text = NSLocalizedString(@"Tap to close", nil);
	closeLabel.font = fontHiragino;
	

	
	//季節に応じた色を、それぞれのラベルに設定
	switch (isSeason(month)) {
		case OPENING:
			messageLabel.textColor = [UIColor whiteColor];
			messageLabel.shadowColor = [UIColor blackColor];
			messageLabel.shadowOffset = CGSizeMake(2, 2);
			
			//			newRecordLabel.textColor = [UIColor redColor];
			//			newRecordLabel.shadowColor = [UIColor yellowColor];
			//			newRecordLabel.shadowOffset = CGSizeMake(2, 1);
			//			
			//			recordTimeLabel.textColor = [UIColor redColor];
			//			recordTimeLabel.shadowColor = [UIColor yellowColor];
			//			recordTimeLabel.shadowOffset = CGSizeMake(2, 1);
			
			closeLabel.textColor = [UIColor whiteColor];
			closeLabel.shadowColor = [UIColor whiteColor];
			break;
		
		case TIMEUP:
			messageLabel.textColor = [UIColor redColor];
			messageLabel.shadowColor = [UIColor orangeColor];
			messageLabel.shadowOffset = CGSizeMake(2, -1);
			
//			newRecordLabel.textColor = [UIColor redColor];
//			newRecordLabel.shadowColor = [UIColor yellowColor];
//			newRecordLabel.shadowOffset = CGSizeMake(2, 1);
//			
//			recordTimeLabel.textColor = [UIColor redColor];
//			recordTimeLabel.shadowColor = [UIColor yellowColor];
//			recordTimeLabel.shadowOffset = CGSizeMake(2, 1);
			
			closeLabel.textColor = [UIColor redColor];
			closeLabel.shadowColor = [UIColor orangeColor];
			break;
		
		case SPRING:
			messageLabel.textColor = [UIColor redColor];
			messageLabel.shadowColor = [UIColor magentaColor];
			messageLabel.shadowOffset = CGSizeMake(2, -1);
			
			newRecordLabel.textColor = [UIColor redColor];
			newRecordLabel.shadowColor = [UIColor yellowColor];
			newRecordLabel.shadowOffset = CGSizeMake(2, 1);
			
			recordTimeLabel.textColor = [UIColor redColor];
			recordTimeLabel.shadowColor = [UIColor yellowColor];
			recordTimeLabel.shadowOffset = CGSizeMake(2, 1);
			
			closeLabel.textColor = [UIColor redColor];
			closeLabel.shadowColor = [UIColor redColor];								
			break;
		
		case SUMMER:
			messageLabel.textColor = [UIColor cyanColor];
			messageLabel.shadowColor = [UIColor blueColor];
			messageLabel.shadowOffset = CGSizeMake(2, 1);
			
			newRecordLabel.textColor = [UIColor yellowColor];
			newRecordLabel.shadowColor = [UIColor blackColor];
			newRecordLabel.shadowOffset = CGSizeMake(2, 2);
			
			recordTimeLabel.textColor = [UIColor yellowColor];
			recordTimeLabel.shadowColor = [UIColor blackColor];
			recordTimeLabel.shadowOffset = CGSizeMake(2, 2);

			
			closeLabel.textColor = [UIColor whiteColor];								
			closeLabel.shadowColor = [UIColor whiteColor];								
			break;

		case AUTUMN:
			messageLabel.textColor = [UIColor whiteColor];
			messageLabel.shadowColor = [UIColor blackColor];
			messageLabel.shadowOffset = CGSizeMake(2, 2);
			
			newRecordLabel.textColor = [UIColor yellowColor];
			newRecordLabel.shadowColor = [UIColor blackColor];
			newRecordLabel.shadowOffset = CGSizeMake(2, 2);
			
			recordTimeLabel.textColor = [UIColor yellowColor];
			recordTimeLabel.shadowColor = [UIColor blackColor];
			recordTimeLabel.shadowOffset = CGSizeMake(2, 2);
			
			closeLabel.textColor = [UIColor cyanColor];								
			closeLabel.shadowColor = [UIColor cyanColor];								
			break;
		
		case WINTER:
			messageLabel.textColor = [UIColor whiteColor];
			messageLabel.shadowColor = [UIColor blackColor];
			messageLabel.shadowOffset = CGSizeMake(2, 2);
			
			newRecordLabel.textColor = [UIColor redColor];
			newRecordLabel.shadowColor = [UIColor magentaColor];
			newRecordLabel.shadowOffset = CGSizeMake(2, 2);
			
			recordTimeLabel.textColor = [UIColor colorWithRed:0 green:0.5 blue:0.25 alpha:1];
			recordTimeLabel.shadowColor = [UIColor greenColor];
			recordTimeLabel.shadowOffset = CGSizeMake(2, 2);
			
			closeLabel.textColor = [UIColor blackColor];								
			closeLabel.shadowColor = [UIColor clearColor];								
			break;
			
	}	
		
	////////////////////////////////////////////
	//クリアタイム ＆　おめでとうor残念メッセージ表示

	//オープニングのとき、タイトル画面表示
	if (openingFlag){
		//ガイダンスフラグをセット
		//オープニング直後、かつ、初めてのプレイ時であれば、アニメーションガイダンスをスタートさせる。
		openingImmediatelyLaterFlag = YES;
		
		
		[messageLabel setLineBreakMode:UILineBreakModeWordWrap];//改行モード
		[messageLabel setNumberOfLines:0];
		messageLabel.text = NSLocalizedString(@"Math Master", nil);
		if (applicationDelegate.isJapanease){	//「あんざんめいじん」と、ふりがなも表示
			phoneticLabel.hidden = NO;
			phoneticLabel.textColor = [UIColor whiteColor];
			phoneticLabel.shadowColor = [UIColor blackColor];
			phoneticLabel.shadowOffset = CGSizeMake(2, 2);
			phoneticLabel.text = @"あんざんめいじん";
			phoneticLabel.font = [UIFont fontWithName:@"HiraKakuProN-W3" size:24];

			messageLabel.font = [UIFont fontWithName:@"HiraKakuProN-W3" size:34];
		} else {
			phoneticLabel.hidden = YES;
		}
	} else {
		phoneticLabel.hidden = YES;
	}
	
	//クリアのとき
	if (0 < clearTime && clearTime <= 60){
		messageLabel.hidden = NO;
		recordTimeLabel.hidden = NO;
	} else {
		messageLabel.hidden = NO;
		recordTimeLabel.hidden = YES;
	}
	//順位表示
	if (rank > 0){
		newRecordLabel.hidden = NO;
	} else {
		newRecordLabel.hidden = YES;
	}
	//closeラベル表示
	closeLabel.hidden = NO;
	
	//答えを表示しておく。
	currentAnswer.text = [[[@"=" stringByAppendingString:operand1Label.text] stringByAppendingString:kindToString(arithmeticKind)] stringByAppendingString:operand2Label.text];
	currentAnswer.hidden = NO;
}

// 達成ビューをタップしたときに、見えなくする。
- (void) congratulationViewTouched {
	
	//確認
	//	NSLog(@"guidanceFlag:%d openingImmediatelyLaterFlag:%d", guidanceFlag, openingImmediatelyLaterFlag);
	
	//暗算画面で吹き出しがでているときもあるので、見えないようにしておく。
	speechBalloonView.hidden = YES;
	
	//達成画面を見えなくする。
	congratulationView.hidden = YES;
	
	
	//達成画面の音楽がでていたら消す。
	if (fanfarePlayer.playing){
		[fanfarePlayer stop];
		fanfarePlayer.currentTime = 0;	//次回のために最初から成るようにしておく。
	}
	if (consolationPlayer.playing){
		[consolationPlayer stop];
		consolationPlayer.currentTime = 0;
	}
	
	
	if (backGroundMusic){
		if (!bgmPlayer.playing) {
			//もしBGM有効になっていたら、BGM再生
			//			NSLog(@"congratulationViewTouched BGM play");
			bgmPlayer.numberOfLoops = -1;	//無限に繰り返す
			bgmPlayer.currentTime = 0;	//最初から
			[bgmPlayer play];
			playing = YES;
		}
	}
	
	if (openingImmediatelyLaterFlag == YES &&
		guidanceFlag == YES){
		
		//プレイボタン等々にタップできないように準備
		maskViewForAll.hidden = NO;
		maskViewForAll.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
		
		//一秒待ってから
		[UIView animateWithDuration:1 delay:0 options:0 animations:nil completion:nil];
		//ガイダンススタート
		[self guidanceStart:nil];
		
	} else {
		guidanceView.hidden = YES;		
	}
	
}


#pragma mark - Play


//記録をCoreDataに保存するメソッド
- (int) addRecord:(double)clearTime {
	//アプリケーションデリゲートポインタを取得
	managedObjectContext = applicationDelegate.managedObjectContext;
	
	//プレイデータを元に、イベント作成
	Entity *event = (Entity *)[NSEntityDescription insertNewObjectForEntityForName:@"RecordEntity" inManagedObjectContext:managedObjectContext];

	[event setClearTime:[NSNumber numberWithDouble:clearTime]];
	[event setDigitOfNumbers:[NSNumber numberWithInt:digitOfNumbers]];
	[event setClearDate:[NSDate date]];
	[event setArithmeticKind:[NSNumber numberWithInt:arithmeticKind]];
	[event setHelpLevel:[NSNumber numberWithInt:helpLevel]];
	
//	NSLog(@"event:%@",event);
	
	//ここから本番
	//フェッチ要求の作成
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"RecordEntity" inManagedObjectContext:managedObjectContext];
	[request setEntity:entity];
	
	//抽出条件をセット
	NSPredicate *predicate = [NSPredicate predicateWithFormat: 
							  [NSString stringWithFormat:@"arithmeticKind == %d", arithmeticKind]]; 	
	[request setPredicate:predicate];
	
	
	//ソート記述子の設定
	NSSortDescriptor *sortDescriptorTime = [[NSSortDescriptor alloc] initWithKey:@"clearTime" ascending:YES];
	NSSortDescriptor *sortDescriptorDate = [[NSSortDescriptor alloc] initWithKey:@"clearDate" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorTime, sortDescriptorDate, nil];
	//先にtimeを記述しているので、time, dateの順でソートされる。
	[request setSortDescriptors:sortDescriptors];
	
	[sortDescriptors release];
	[sortDescriptorTime release];
	[sortDescriptorDate release];
	
	//10件だけ抽出する & 溢れた記録があるかもしれないので＋1している
	[request setFetchLimit:RECORD_MAX + 1];
	
	//フェッチ要求の実行
	NSError *error = nil;
	NSMutableArray *mutableFetchResults = [[managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
	if (mutableFetchResults == nil) {
		// エラーを処理する
	}
	
	//件数を出してみる。
//	NSLog(@"mutable count %d",[mutableFetchResults count]);
	
	//終了処理
	[self setEventsArray:mutableFetchResults];	//ソートされたフェッチ結果が格納される
	[mutableFetchResults release];
	[request release];
	
	//	なので件数を出してみる。
//	NSLog(@"eventsarray count %d",[eventsArray count]);
	
	//保存している記録が10件超過していたら、その分（きっと一つ）を削除する
	// 指定のインデックスパスにある管理オブジェクトを削除する。
	while ([eventsArray count] > RECORD_MAX){
//		NSLog(@"count naka %d", [eventsArray count]);
		NSManagedObject *eventToDelete = [eventsArray objectAtIndex:RECORD_MAX];
		[managedObjectContext deleteObject:eventToDelete];
		// 配列とViewを更新する。
		[eventsArray removeObjectAtIndex:RECORD_MAX];
		
		// 変更をコミットする。
		NSError *error = nil;
		if (![managedObjectContext save:&error]) {
			// エラーを処理する。
		}
	}
	// 変更をコミットする。
	error = nil;
	if (![managedObjectContext save:&error]) {
		// エラーを処理する。
	}
//	NSLog(@"count ato %d", [eventsArray count]);
	
	//何番目の記録か、記録日時をキーに検索、返す
	int rank = -1;
	if ([event clearDate] == NULL){
		//ランク外なら、displayRecordの中で削除されているはずので、nullと比較している。
		rank = -1;	//-1ならランク外の意味
	} else {
		for (int i = 0; i < [eventsArray count] && i < RECORD_MAX; i++){ 
			Entity *entity = (Entity *)[eventsArray objectAtIndex:i];
//			NSLog(@"%d: event:%@ entity:%@", i, [event clearDate], [entity clearDate]);
			if ([[event clearDate] compare:[entity clearDate]] == NSOrderedSame){
				rank = i + 1;
				break;
			}
		}
	}
//	NSLog(@"addrecord rank %d !!", rank);
	
	return rank;
}



//暗算画面に時間表示を行うメソッド
//スタートからの時間を計測して、プログレスバーをセットする。
- (void) displayTimeSet {

	//残り時間を取得し、表示。
	remainingTime = 60.0 - [[NSDate date] timeIntervalSinceDate:startTime]; //残り時間
	timeDisplayLabel.text = [NSString stringWithFormat:@"0:%06.3f", remainingTime];
	timeProgressBar.progress = (float)(remainingTime / 60.0f);

	//画面の色の変更
	if (30 <= remainingTime){
//		displayAreaColorView.backgroundColor = [UIColor cyanColor];
//		displayAreaColorView.alpha = 0.8;
		displayAreaColorView.backgroundColor = [UIColor colorWithRed:0.4 green:0.7 blue:1 alpha:1];
	} else if (10 <= remainingTime && remainingTime < 30){
//		displayAreaColorView.backgroundColor = [UIColor orangeColor];
//		displayAreaColorView.alpha = 0.8;
		displayAreaColorView.backgroundColor = [UIColor colorWithRed:1 green:0.5 blue:0 alpha:1];
	} else {
//		displayAreaColorView.backgroundColor = [UIColor redColor];
//		displayAreaColorView.alpha = 0.8;
		displayAreaColorView.backgroundColor = [UIColor colorWithRed:1 green:0 blue:0 alpha:1];
	}
	
}

//正常な入力状態か否かチェックを行う。
//異常ならば吹き出しを表示する。
//戻り値は、正常ならば、YES 異常ならばNO
- (BOOL) isPass {
	// ユーザーが入力した文字列の中に演算子が存在するか否か
	int num1;
	int num2;

	//通常は隠しておく。
	speechBalloonView.hidden = YES;
	
	//演算子で分割
	NSString *operatorString = kindToString(arithmeticKind);
	NSArray *array = [userInputDataDisplayLabel.text componentsSeparatedByString:operatorString];
	
	//演算子が先頭に来ることを禁止
	if ([userInputDataDisplayLabel.text length] > 0 && [userInputDataDisplayLabel.text isEqualToString:kindToString(arithmeticKind)]){
		
		if (applicationDelegate.isJapanease){
			speechBalloonLabel.numberOfLines = 0;
			speechBalloonLabel.text = @" すうじを\n さきにいれて";
		} else {
			speechBalloonLabel.text = [@" " stringByAppendingString: [kindToString(arithmeticKind) stringByAppendingString:NSLocalizedString(@" Not First", nil)]];
		}
		speechBalloonView.hidden = NO;
		return NO;

	}

	//複数の演算子を使うことを禁止
//	if ([array count] >= 3){
//		NSLog(@"count3 isPass array %@", array);

	if (pluralOperator == YES){
		if (applicationDelegate.isJapanease){
			speechBalloonLabel.numberOfLines = 0;
			speechBalloonLabel.text = [@" " stringByAppendingString:[kindToString(arithmeticKind) stringByAppendingString:@"は一つだけ"]];
		} else {
			speechBalloonLabel.text = [@" " stringByAppendingString:[kindToString(arithmeticKind) stringByAppendingString:NSLocalizedString(@" One Use Only", nil)]];
		}
		speechBalloonView.hidden = NO;
		return NO;

	}
	
	//0が先頭に来ることを禁止 第一被演算子
	if ([array count] >= 1 && [[array objectAtIndex:0] length] > 0){
		NSString *str1 = [[array objectAtIndex:0] substringToIndex:1];
		if ([str1 isEqualToString:@"0"]){
			if (applicationDelegate.isJapanease){
				speechBalloonLabel.numberOfLines = 0;
				speechBalloonLabel.text = @" はじめに0は\n こないよ";
			} else {
				speechBalloonLabel.text = @" 0 Not First";
			}
			speechBalloonView.hidden = NO;
			return NO;
		}
	}
	
	//0が先頭に来ることを禁止　第二被演算子
	if ([array count] >= 2 && [[array objectAtIndex:1] length] > 0){
		NSString *str2 = [[array objectAtIndex:1] substringToIndex:1];
		if ([str2 isEqualToString:@"0"]){
			if (applicationDelegate.isJapanease){
				speechBalloonLabel.numberOfLines = 0;
				speechBalloonLabel.text = @" はじめに0は\n こないよ";
			} else {
				speechBalloonLabel.text = @" 0 Not First";
			}
			speechBalloonView.hidden = NO;
			return NO;
		}
	}

	
	num1 = [[array objectAtIndex:0] intValue];
	if ([array count] >= 2){
		num2 = [[array objectAtIndex:1] intValue];
	} else {
		num2 = -1;
	}
	questionNumber = [questionLabel.text intValue];
	//演算子を使わず、直接お題の数字をタップしている場合
	if ([array count] == 1 && num1 == questionNumber) {
		if (applicationDelegate.isJapanease){
			speechBalloonLabel.numberOfLines = 0;
			speechBalloonLabel.text = [@" " stringByAppendingString:[kindToString(arithmeticKind) stringByAppendingString:@"も\n つかってね"]];
		} else {
			speechBalloonLabel.text = [@" " stringByAppendingString:[kindToString(arithmeticKind) stringByAppendingString:NSLocalizedString(@" Use please", nil)]];
		}
		speechBalloonView.hidden = NO;
		return NO;
	}

//	//お題の数字をタップした直後に演算子を入れている場合
//	if ([array count] >= 2 && [[array objectAtIndex:1] length] == 0 && num1 == questionNumber){
//		speechBalloonView.hidden = YES;
//		
//		
//		if (helpLevel <= NORMAL){
//			currentAnswer.text = [array objectAtIndex:0];
//			currentAnswer.hidden = NO;
//		}
//		
//		return NO;
//	}		

	//2桁以上の乗算において、第1,2被演算子の1は許可しない。
	if (arithmeticKind == MULTIPLICATION) {
		if (digitOfNumbers >= 2 && (num1 == 1 || num2 == 1) ){
			if (applicationDelegate.isJapanease){
				speechBalloonLabel.numberOfLines = 0;
				speechBalloonLabel.text = @" ほかのかずに\n してね";
			} else {
				speechBalloonLabel.text = [@" " stringByAppendingString:NSLocalizedString(@"1 don't pass.", nil)];
			}
			speechBalloonView.hidden = NO;
			return NO;
		}
	}
	//2桁以上の除算において、第二被演算子の1は許可しない。
	if (arithmeticKind == DIVISION) {
		if (digitOfNumbers >= 2 && num2 == 1){
			if (applicationDelegate.isJapanease){
				speechBalloonLabel.numberOfLines = 0;
				speechBalloonLabel.text = @" ほかのかずを\n つかってね";
			} else {
				speechBalloonLabel.text = [@" " stringByAppendingString:[kindToString(arithmeticKind) stringByAppendingString:NSLocalizedString(@"1 don't pass.", nil)]];
			}
			speechBalloonView.hidden = NO;
			return NO;
		}
	}
	
	//それ以外の場合は正常な入力なので、
	return YES;
	
	
	
	
}


// 正解しているかどうかの判定
// 及び、達成画面の表示を行うメソッド
-(void)timeCheck:(NSTimer *)tm	//tmには呼び出し元のインスタンスオブジェクトが渡されている
{
	
	double clearTime = 60 - remainingTime;
	
	//６０秒でクリアできなかったら、残念画面の表示
	if (clearTime > 60){
		
		//マイナスなんて表示されるとかっこわるいので。
		timeDisplayLabel.text = @"0:00.000";
		
		//タイマー停止処理
		[self displayTimerStop];
		[self decisionTimerStop];
		
		//スタッククリア
		[chipStack removeAllObjects];
		
		//残念画面の表示処理
		int rank = -1;	//ダミーデータ
		[self showCongratulationClearTime:clearTime rank:rank opening:NO];
		return;
	}


	//大きすぎる数を入力していないか
	if ([self tooBig] == YES){
		return;
	}
	
	//きちんとした入力をしているかどうか、確認
	BOOL isPassAnswer = [self isPass];
//	if (isPassAnswer == NO){
//		return;
//	}
	
	//入力された答えが合っているか確認する。
	NSString *operatorString = kindToString(arithmeticKind);
	
	//入力された値が正解かどうか判定するために、計算してみる。
	
	
//	NSLog(@"userInputDataDisplayLabel.text: %@", userInputDataDisplayLabel.text);
	NSArray *array = [userInputDataDisplayLabel.text componentsSeparatedByString:operatorString];
	NSInteger num1 = [[array objectAtIndex:0] intValue];
	NSInteger num2;
	if ([array count] >= 2 && ![[array objectAtIndex:1] isEqualToString:@""]){
		num2 = [[array objectAtIndex:1] intValue];
	} else {
		num2 = UN_INPUTTED;	//未入力の意味
	}
	switch (arithmeticKind) {
		case ADDITION:
			answerNumber =  num1 + num2;
			break;
		case SUBTRACTION:
			answerNumber =  num1 - num2;
			break;
		case MULTIPLICATION:
			answerNumber =  num1 * num2;
			break;
		case DIVISION:
			if (num2 != 0 && num2 != UN_INPUTTED){
				answerNumber =  num1 / num2;
			} else {
				answerNumber = UN_INPUTTED;
			}
			break;
	}
	
	
	//お助けモードの実装
	//ヘルプレベルに応じてヒントを表示する。
	
	if (helpLevel == EASY && 
		(arithmeticKind == ADDITION || arithmeticKind == SUBTRACTION)){
		currentAnswer.hidden = NO;
//		[self addUnderBar];	//すでにaddUndarBarメソッド内で表示されているので、上書きしない
	} 
	else if (helpLevel <= NORMAL && isPassAnswer == YES){
		if (num2 != 0 && num2 != UN_INPUTTED && answerNumber != UN_INPUTTED){
			currentAnswer.text = [NSString stringWithFormat:@"%d", answerNumber];
//			NSLog(@"answerNumber:%d num2:%d", answerNumber, num2);
		} else {
			currentAnswer.text = [NSString stringWithFormat:@"%d", num1];
//			NSLog(@"currentAnswer:%d num2:%d", num1, num2 );
		}
		currentAnswer.hidden = NO;
	} else {
		currentAnswer.text = @"";
		currentAnswer.hidden = YES;
	}
	

//	NSLog(@"answer %d, question %d, isPass %d", answerNumber, questionNumber, isPassAnswer);
	
	questionNumber = [questionLabel.text intValue];
	
	//正解しているときの処理
	if (answerNumber == questionNumber && isPassAnswer == YES){	//＋0や（2桁以上の場合の）×1など
											//意図していない回答のチェックは以前にしている。

		//正解として、タイマー停止処理
		[self displayTimerStop];
		[self decisionTimerStop];

		//記録を追加
		//もし追加してランク外だったらあわせて削除も行われる。
		int rank = [self addRecord:clearTime];

		//スタッククリア
		[chipStack removeAllObjects];
		
		//達成画面の表示処理
		[self showCongratulationClearTime:clearTime rank:rank opening:NO];
		
		return;
	}
	

}



- (BOOL) tooBig {
	//大きすぎる桁数を入力されたときに弾く。
	
	NSArray *array = [userInputDataDisplayLabel.text componentsSeparatedByString:kindToString(arithmeticKind)];
//	NSLog(@"tooBig array %@", array);
	NSString *str1 = [array objectAtIndex:0];
	
	NSString *str2 = @"";
	if ([array count] >= 2){
		str2 = [array objectAtIndex:1];
	}
	
	switch (arithmeticKind) {
		case ADDITION:
		case SUBTRACTION:
			if ([str1 length] >= 6 || [str2 length] >= 6){
				speechBalloonView.hidden = NO;
				speechBalloonLabel.numberOfLines = 0;
				speechBalloonLabel.text = NSLocalizedString(@"Too Big", nil);
				return YES;
			}
			break;
		case MULTIPLICATION:
			if ([str1 length] >= 5 || [str2 length] >= 5){
				speechBalloonView.hidden = NO;
				speechBalloonLabel.numberOfLines = 0;
				speechBalloonLabel.text = NSLocalizedString(@"Too Big", nil);
				return YES;
			}
			break;
		case DIVISION:
			if ([str1 length] >= 8 || [str2 length] >= 8){
				speechBalloonView.hidden = NO;
				speechBalloonLabel.numberOfLines = 0;
				speechBalloonLabel.text = NSLocalizedString(@"Too Big", nil);
				return YES;
			}
			break;
	}

	return NO;
	
}

// チップに触れたときの処理いろいろ
- (void) chipTouched:(UIButton *)button {

	//音を出してみる
	[self tapSE_Play];
	
	//既に押したチップか否か
	if ([chipStack containsObject:button]){
		if (button.tag == ((UIButton*)[chipStack peek]).tag){
			//直前に触れたチップならば、タップしたことを取り消せる。
			[self backspaceKey:nil];		
			return;
		} else {
			//直前に押したチップでない、使用済みチップを押した場合は、何もせず、戻る。
			return;
		}
	}
	
	else {
	
	
		//大きすぎる数を弾く
		if ([self tooBig] == YES){
			return;
		}
		
		//	if ([self isPass] == NO){
		//		return;
		//	}
		
		speechBalloonView.hidden = YES;
		

		pluralOperator = NO;
		//演算子か否かチェック
		if ([button.titleLabel.text isEqualToString:kindToString(arithmeticKind)]){

			//演算子ボタンだったとき
			// ユーザーの入力文字列中に演算子が含まれているか確認
			NSString *str = userInputDataDisplayLabel.text;
			NSRange searchResult = [str rangeOfString:kindToString(arithmeticKind)];
			if(searchResult.location != NSNotFound){
				// 既に演算子が含まれていたならば
				pluralOperator = YES;
			}else{
				// 初めての演算子ならば
				pluralOperator = NO;
			}
		}

		if (pluralOperator == NO){
			//チップスタックにプッシュ
			[chipStack push:button];
			
			//ボタンの色を変更
			[button setBackgroundImage:[UIImage imageNamed:@"imageUsed.png"] forState:UIControlStateNormal]; 
			//	NSLog(@"chipStack length: %d", [chipStack length]);
			
			//答フィールドに表示
			userInputDataDisplayLabel.text = [[[userInputDataDisplayLabel.text componentsSeparatedByString:@"_"] objectAtIndex:0] stringByAppendingString:button.titleLabel.text];
			userInputDataDisplayLabel.font = [UIFont systemFontOfSize:24];	//24 point
			
			//### 開発中 ###
			
			
			if (helpLevel == EASY && 
				(arithmeticKind == ADDITION || arithmeticKind == SUBTRACTION)){
				[self addUnderBar];
			}
		}
	}
	
}




//num2の入力中、アンダーバーを追加するメソッド
//お題が1234
//num1が200のとき
//num2が1ならば、
//200+1___と表示する
//加えて、現在入力中の計算結果として、
//1200(=200+1000)と表示するメソッド
- (void) addUnderBar {
	//入力フィールド分割
	NSArray *array = [userInputDataDisplayLabel.text componentsSeparatedByString:kindToString(arithmeticKind)];
	NSString *str1;
	NSString *str2;
	str1 = [array objectAtIndex:0];
	
	NSInteger num1 = [str1 intValue];
	NSInteger num2;
	
	BOOL operatorUsed;
	if ([array count] == 1){
		operatorUsed = NO;
	} else {
		operatorUsed = YES;
	}
	
	if ([array count] >= 2 && ![[array objectAtIndex:1] isEqualToString:@""]){
		str2 = [array objectAtIndex:1];
		str2 = [self trimUnderBar:str2];
		num2 = [str2 intValue];
	} else {
		str2 = @"UN_INPUTTED";	//未入力の意味
		num2 = UN_INPUTTED;		//未入力の意味
	}
	
	
	
	NSInteger appendDigit = 0;
	if (num2 != UN_INPUTTED){
		//追加する
		if (arithmeticKind == ADDITION){
			appendDigit = digit(questionNumber-num1) - digit(num2);
			str2 = [self appendUnderBar:str2 number:appendDigit];
		} else if (arithmeticKind == SUBTRACTION){
			appendDigit = digit(num1-questionNumber) - digit(num2);
			str2 = [self appendUnderBar:str2 number:appendDigit];
		}
//		NSLog(@"str2: %@", str2);
	}
	num2 = appendDigit >= 0 ? num2 * pow(10, appendDigit) : num2;
//	NSLog(@"num2: %d", num2);
	
	//結合
	NSString *newText = @"";
	if (operatorUsed == NO){
		newText = str1;
	}
	if (operatorUsed == YES && [str2 isEqualToString:@"UN_INPUTTED"]){
		newText = [str1 stringByAppendingString:kindToString(arithmeticKind)];
	}
	if (operatorUsed == YES && ![str2 isEqualToString:@"UN_INPUTTED"]){
		newText = [[str1 stringByAppendingString:kindToString(arithmeticKind)] stringByAppendingString:str2];
	}

//	NSLog(@"newText:%@", newText);
//	NSLog(@"num1: %d num2: %d", num1, num2);

	userInputDataDisplayLabel.text = newText;
	if (arithmeticKind == ADDITION){
		if (num2 != UN_INPUTTED){
			currentAnswer.text = [NSString stringWithFormat:@"%d", (num1 + num2)];
		} else {
			currentAnswer.text = str1;
		}
	} else if (arithmeticKind == SUBTRACTION) {
		if (num2 != UN_INPUTTED){
			currentAnswer.text = [NSString stringWithFormat:@"%d", (num1 - num2)]; 
		} else {
			currentAnswer.text = str1;
		}
		
	}
	
}

			 
			 
- (NSString *) appendUnderBar:(NSString *)str number:(int)num {
	
	for (int i = 1; i <= num; i++){
		str = [str stringByAppendingString:@"_"]; 
	}
	
	return str;
}

- (NSString *) trimUnderBar:(NSString *)str {
//		NSArray *array = [userInputDataDisplayLabel.text componentsSeparatedByString:kindToString
	
	NSArray *array = [str componentsSeparatedByString:@"_"];
	
	return [array objectAtIndex:0]; 
}

int digit (int n){
	if (n < 10){
		return 1;
	} else if (n < 100) {
		return 2;
	} else if (n < 1000) {
		return 3;
	} else if (n < 10000) {
		return 4;
	} else if (n < 100000) {
		return 5;
	} else if (n < 1000000) {
		return 6;
	} else {
		return 7;
	}
}




- (IBAction)backspaceKey:(id)sender {
	
	//ボタンを押したことを分かりやすく
	[self tapSE_Play];
	
	
	UIButton *button = [chipStack pop];
	
	
	//ボタンの色を元に戻す	
	switch (arithmeticKind) {
		case ADDITION:
			[button setBackgroundImage:[UIImage imageNamed:@"imageAdd.png"] forState:UIControlStateNormal];
			break;
		case SUBTRACTION:
			[button setBackgroundImage:[UIImage imageNamed:@"imageSub.png"] forState:UIControlStateNormal];
			break;
		case MULTIPLICATION:
			[button setBackgroundImage:[UIImage imageNamed:@"imageMul.png"] forState:UIControlStateNormal];
			break;
		case DIVISION:
			[button setBackgroundImage:[UIImage imageNamed:@"imageDiv.png"] forState:UIControlStateNormal];
			break;
	}
	

	//_がついている場合に削除してから、数字を削除する必要が有る。

	NSString *str = userInputDataDisplayLabel.text;	

	str = [[str componentsSeparatedByString:@"_"] objectAtIndex:0];
	
	if ([str length] != 0){
		str = [str substringWithRange:NSMakeRange(0,[str length]-1)];
		userInputDataDisplayLabel.text = str;
	}	

	
	
	//アンダーバー付加
	if (helpLevel == EASY && 
		(arithmeticKind == ADDITION || arithmeticKind == SUBTRACTION)){
		[self addUnderBar];
	}

	
}



// プレイボタンを押したとき
- (IBAction)play:(id)sender {
	
	//カウントダウン前にお題が見えないように、透明にしておく
	questionLabel.textColor = [UIColor clearColor];
	currentAnswer.textColor = [UIColor clearColor];
	userInputDataDisplayLabel.textColor = [UIColor clearColor];
	
	//カウントダウンのために画面を暗くする
	[self maskViewSetForCountDown:YES];
	//カウントダウンスタート　3、2、1のあと始まる。
	countDownNumber = 3;
	[self countDownTimerStart];

	//タイマーが動いているなら停止しておく
	[self displayTimerStop];
	[self decisionTimerStop];
	
	//次回のプレイのために回答欄を空白にしておく
    userInputDataDisplayLabel.text = @"";           //回答欄を空白に
	//ボタンを押したときに音を出させない。
	isValidSE = NO;
	//お題を設定するとともに、お題が解けるようにチップを配置する。
	[self questionAndChipAssignmentOpening:NO];
	//ボタンを押したときに音を出させる。
	isValidSE = YES;

	//現在入力中の答えも表示しておく
	if (helpLevel == EASY){
		currentAnswer.text = [userInputDataDisplayLabel.text substringWithRange:NSMakeRange(0,[userInputDataDisplayLabel.text length]-1)];
	} else {
		currentAnswer.text = @"";
	}

	
	//チップボタンに触れられるようにする。
	[self chipsUserInteractionEnabled:YES];
	//タップ音が出ても良いようにする。
	isValidSE = YES;
	
}

- (void) chipsUserInteractionEnabled:(BOOL)yesOrNo {
	for (UIButton *button in chipArray){
		button.userInteractionEnabled = yesOrNo;
	}
	[self maskViewSetForUnTouched:!yesOrNo];
}


//画面にチップを並べておく。（このチップでプレイする訳ではなく、見た目のために）
- (void) chipAssignmentForOpeningAndSettingChanged:(BOOL)opening {
	
	//演算子の種類と、お題の桁数を、ユーザーデフォルトから読み込む
	[self loadUserDefault];
	
	//画面表示設定いろいろ
	displayAreaColorView.layer.masksToBounds = YES;
	displayAreaColorView.layer.cornerRadius = 15;
	timeProgressBar.progress = 1.0;
	chipStack = [[NSMutableArray alloc] initWithCapacity:1];
//	displayAreaColorView.backgroundColor = [UIColor cyanColor];
//	displayAreaColorView.alpha = 0.8;
	displayAreaColorView.backgroundColor = [UIColor colorWithRed:0.4 green:0.7 blue:1 alpha:1];
	timeDisplayLabel.text = @"1:00.000";
	currentAnswer.text = @"";

	
	//初期配置する際に、タップ音がでないようにする。
	isValidSE = NO;
	//オープニング画面を閉じたときにチップが配置されていて欲しいので、呼び出す。
	//（設定変更に伴うチップ再配置時にもつかえる);	
	[self questionAndChipAssignmentOpening:opening];
	[self chipsUserInteractionEnabled:NO];
	
	//questionAndChipAssignmentメソッドの副作用を初期化する
	operand1Label.text = @"";
	operand2Label.text = @"";
	userInputDataDisplayLabel.text = @"";
	questionLabel.text = @"";
	while ([chipStack count] != 0){
		UIButton *button = [chipStack lastObject];
		[self chipTouched:button];
	}
	[needsChipList removeAllObjects];

	[self chipsUserInteractionEnabled:NO];
	maskViewForAll.hidden = YES;

}


#pragma mark - Record / Help / Setting
// 記録ビューからのデリゲート
- (void)recordViewControllerDidFinish:(RecordViewController *)controller {
    [self dismissModalViewControllerAnimated:YES];
}

// ヘルプビューからのデリゲート
- (void)helpViewControllerDidFinish:(HelpViewController *)controller {
    [self dismissModalViewControllerAnimated:YES];
}

// 設定画面からのデリゲート
- (void)settingViewControllerDidFinish:(SettingViewController *)controller 								   settingChanged:(BOOL)settingChanged {

	//設定変更があったなら、
	//プレイ中ならプレイ中止、画面にチップ再配置
	//プレイ中でなくても、　　画面にチップ再配置
	
	if (settingChanged){
		[self displayTimerStop];
		[self decisionTimerStop];
		
		[self chipAssignmentForOpeningAndSettingChanged:NO];
	}
	
	[self dismissModalViewControllerAnimated:YES];
}

- (IBAction)showRecord:(id)sender
{
    RecordViewController *controller = [[RecordViewController alloc] initWithNibName:@"RecordView" bundle:nil];
    controller.delegate = self;
    
    controller.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentModalViewController:controller animated:YES];
    
    [controller release];
}



- (IBAction)showHelp:(id)sender {
	
	HelpViewController *controller = [[HelpViewController alloc] initWithNibName:@"HelpView" bundle:nil];
	controller.delegate = self;
	
	controller.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
	[self presentModalViewController:controller animated:YES];
	
	[controller release];
}

- (IBAction)showSetting:(id)sender {

	SettingViewController *controller = [[SettingViewController alloc] initWithNibName:@"SettingView" bundle:nil];
	controller.delegate = self;
	
	controller.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
	[self presentModalViewController:controller animated:YES];
	
	[controller release];
}






#pragma mark - view lifecycle

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
	
	//ステータスバー非表示
	[[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];

	//アプリケーションデリゲートポインタを取得
	applicationDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	
	//言語環境の設定
	[applicationDelegate confirmLanguage];
	
	//timer初期化
	displayTimer = nil;
	decisionTimer = nil;
	countDownTimer = nil;
	
	//ローカライズ
	questionTitleLabel.text = NSLocalizedString(@"Question", nil);
	answerTitleLabel.text = NSLocalizedString(@"Answer", nil);
	
	//音の用意
	[self setupApplicationBGM];
	
	//オープニング画面表示
	
	//BGMを出す処理は、showCongratulationメソッドに記述
	congratulationView.hidden = NO;
	[self showCongratulationClearTime:-1 rank:-1 opening:YES];
	
	//画面にチップを並べておく。（このチップでプレイする訳ではなく、見た目のために）
	[self chipAssignmentForOpeningAndSettingChanged:YES];

	
	//達成画面（ビュー）閉じる為に、ジェスチャレコグナイザの生成
	UITapGestureRecognizer *aFinger1TapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(congratulationViewTouched)];
	aFinger1TapRecognizer.numberOfTapsRequired = 1;							//シングルタップ
	aFinger1TapRecognizer.numberOfTouchesRequired = 1;						//一本指
	[self.congratulationView addGestureRecognizer:aFinger1TapRecognizer];	//達成画面に登録
	[aFinger1TapRecognizer release];

	//プログレスバー　ちょっと太めに。
	timeProgressBar.transform = CGAffineTransformMakeScale(1, 1.5);

	//最初は吹き出しは消しておく
	speechBalloonView.hidden = YES;

	//角を丸くしておく
	guidanceTextView.layer.masksToBounds = YES;
	guidanceTextView.layer.cornerRadius = 15;
	
//	NSLog(@"viewdidload %d guidanceFlag", guidanceFlag);

}



// ビューが再表示されたときに呼ばれる。
- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	
	//ステータスバー非表示
	[[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];

	[self loadUserDefault];
//	[self showUserDefault:@"viewWillAppear"];
	
	if (backGroundMusic){
		//もしBGM有効になっていたら、BGM再生
		bgmPlayer.numberOfLoops = -1;	//無限に繰り返す
		[bgmPlayer play];
		playing = YES;
	} else {
		//もしBGM無効になっていたら、BGM再生しない
		[bgmPlayer stop];
		bgmPlayer.currentTime = 0;	//次回再生時は最初からにしておく。
		playing = YES;
	}
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload
{
	
	//アプリケーションデリゲート
	//どうすべき？？？
//	AppDelegate *applicationDelegate;

	//プログラム内で使用するメンバ変数
	[self setStartTime:nil];
	//double remainingTime　不要
	//NSInteger questionNumber 不要
	//NSInteger answerNumber 不要
	[self setChipArray:nil];
	[self setNeedsChipList:nil];
	[self setChipStack:nil];
	
	//設定のための変数 //皆プリミティブ型なので記述不要
//	ArithmeticKind	arithmeticKind;		//演算子の種類
//	int				digitOfNumbers;		//出題するお題の桁数
//	HelpLevel		helpLevel;			//お助けモードのレベル EASY/NORMAL/HARD
//	BOOL			backGroundMusic;	//BGM
//	BOOL			soundEffect;		//SE
	
	//BGM・SE関係
	[self setBgmURL:nil];
	[self setBgmPlayer:nil];
	[self setTapURL:nil];
	[self setFanfareURL:nil];
	[self setFanfarePlayer:nil];
	[self setConsolationURL:nil];
	[self setConsolationPlayer:nil];
	
//	BOOL			playing;
//	BOOL			interruptedOnPlayback;

	//タップしたときに音を出させないようにする。（初期配置でチップを並べるとき）
//	BOOL			isValidSE;

	//タイマー用
	[displayTimer invalidate];
	displayTimer = nil;
	[decisionTimer invalidate];
	decisionTimer = nil;
	[countDownTimer invalidate];
	countDownTimer = nil;
	[guidanceTimer invalidate];
	guidanceTimer = nil;
	

	//Core Data 用 どこに記述すべき？？？
//	NSManagedObjectContext 	*managedObjectContext;	    
//	NSMutableArray 			*eventsArray;

	//IBが生成するメンバ変数 Outlet
	//ディスプレイエリア　（画面上部1/3)
	[self setQuestionTitleLabel:nil];
	[self setAnswerTitleLabel:nil];
	[self setTimeDisplayLabel:nil];
	[self setTimeProgressBar:nil];
	[self setQuestionLabel:nil];
	[self setUserInputDataDisplayLabel:nil];
	[self setOperand1Label:nil];
	[self setOperand2Label:nil];
	[self setDisplayAreaColorView:nil];
	[self setCurrentAnswer:nil];

	//達成画面（ビュー）
	[self setCongratulationView:nil];
    [self setCongratulationImageView:nil];
	[self setMessageLabel:nil];
	[self setPhoneticLabel:nil];

	[self setNewRecordLabel:nil];
	[self setRecordTimeLabel:nil];
	[self setCloseLabel:nil];

	//チップ（ボタン）群をのせるためのビュー
	[self setChipBackgroundView:nil];
    
	[self setMaskView:nil];
	[self setCountDownLabel:nil];
	[self setMaskViewForAll:nil];
	
	
	//吹き出し
	[self setSpeechBalloonView:nil];
	[self setSpeechBalloonLabel:nil];

	//ガイダンス
	[self setGuidanceView:nil];
	
	[self setSBPlayView:nil];
	[self setSBPlayImageView:nil];
	[self setSBPlayLabel:nil];
	
	[self setSBPlayView:nil];
	[self setSBPlayImageView:nil];
	[self setSBPlayLabel:nil];
	
	[self setSBRecordView:nil];
	[self setSBRecordImageView:nil];
	[self setSBRecordLabel:nil];
	
	[self setSBHelpView:nil];
	[self setSBHelpImageView:nil];
	[self setSBHelpLabel:nil];
	
	[self setSBSettingsView:nil];
	[self setSBSettingsImageView:nil];
	[self setSBSettingsLabel:nil];
	
	[self setGuidanceTextView:nil];
	
	[self setSpeechBalloonImageView:nil];
	[self setGuidanceCloseButton:nil];
    [super viewDidUnload];

}




- (void)dealloc
{
	//アプリケーションデリゲート
//	AppDelegate *applicationDelegate;

	//プログラム内で使用するメンバ変数
	[startTime release];
//	double 			remainingTime;		//残り時間。
//	NSInteger 		questionNumber;		//お題の数字。
//	NSInteger 		answerNumber;		//回答欄に入力された数。
	[chipArray release];
	[needsChipList release];
	[chipStack release];
	
	//設定のための変数
//	ArithmeticKind	arithmeticKind;		//演算子の種類
//	int				digitOfNumbers;		//出題するお題の桁数
//	HelpLevel		helpLevel;			//お助けモードのレベル EASY/NORMAL/HARD
//	BOOL			backGroundMusic;	//BGM
//	BOOL			soundEffect;		//SE

	//BGM・SE関係
	[bgmURL release];
	[bgmPlayer release];
	
	//mask用
	[tapURL release];
	[tapPlayer release];
	[fanfareURL release];
	[fanfarePlayer release];
	[consolationURL release];
	[consolationPlayer release];
	
//	BOOL			playing;
//	BOOL			interruptedOnPlayback;

	//タップしたときに音を出させないようにする。（初期配置でチップを並べるとき）
//	BOOL			isValidSE;

	//タイマー用
	//不要	
//	NSTimer			*displayTimer;	//表示用タイマー
//	NSTimer			*decisionTimer;	//正解判定用タイマー
	
	
	

	//Core Data 用
	[_managedObjectContext release];	//これはいいの？？？
//	NSManagedObjectContext 	*managedObjectContext;	    
	[eventsArray release];
	
	//IBが生成するメンバ変数 Outlet
	//ディスプレイエリア　（画面上部1/3)
	[questionTitleLabel release];
	[answerTitleLabel release];
	[timeDisplayLabel release];
	[timeProgressBar release];
	[questionLabel release];
	[userInputDataDisplayLabel release];
	[operand1Label release];
	[operand2Label release];
	[displayAreaColorView release];
	[currentAnswer release];

	//達成画面（ビュー）
	[congratulationView release];
    [congratulationImageView release];
	[messageLabel release];
	[phoneticLabel release];

	[newRecordLabel release];
	[recordTimeLabel release];
	[closeLabel release];

	//チップ（ボタン）群をのせるためのビュー
	[chipBackgroundView release];
	
	//mask用
	[maskView release];
	[countDownLabel release];
	[maskViewForAll release];
	
	//吹き出し
	[speechBalloonView release];
	[speechBalloonLabel release];
	
	//ガイダンス
	[guidanceView release];
	
    [SBPlayView release];
	[SBPlayImageView release];
	[SBPlayLabel release];
	
	[SBRecordView release];
	[SBRecordImageView release];
	[SBRecordLabel release];
	
	[SBHelpView release];
	[SBHelpImageView release];
	[SBHelpLabel release];
	
	[SBSettingsView release];
	[SBSettingsImageView release];
	[SBSettingsLabel release];
	
	[guidanceTextView release];
	
	[speechBalloonImageView release];
	[guidanceCloseButton release];
	[super dealloc];
}

@end
