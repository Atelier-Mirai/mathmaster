//
//  MainViewController.h
//  Mental ArithmeticKind Master
//
//  Created by 廣瀬 誠 on 平成23/11/10.
//  Copyright 23 岐阜IT協同組合. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <AudioToolbox/AudioServices.h>
#import <AVFoundation/AVFoundation.h>	//BGM再生のために


#import "RecordViewController.h"
#import "HelpViewController.h"
#import "SettingViewController.h"
#import "Constants.h"

#import "AppDelegate.h"

@interface MainViewController : UIViewController
<RecordViewControllerDelegate,
HelpViewControllerDelegate,
SettingViewControllerDelegate,
AVAudioPlayerDelegate>
{

	//アプリケーションデリゲート
	AppDelegate *applicationDelegate;

	//プログラム内で使用するメンバ変数
	NSDate 			*startTime;			//Play開始時刻を保持。秒数の管理用。
	double 			remainingTime;		//残り時間。
	NSInteger 		questionNumber;		//お題の数字。
	NSInteger 		answerNumber;		//回答欄に入力された数。
	NSMutableArray 	*chipArray;			//49チップ（UIButton）を格納する配列
	NSMutableArray	*needsChipList;		//お題を解くために必要なチップ(数枚)を格納
	NSMutableArray	*chipStack;			//どのチップがタップされたか、確認用のスタック

	//設定のための変数
	ArithmeticKind	arithmeticKind;		//演算子の種類
	int				digitOfNumbers;		//出題するお題の桁数
	HelpLevel		helpLevel;			//お助けモードのレベル EASY/NORMAL/HARD
	BOOL			backGroundMusic;	//BGM
	BOOL			soundEffect;		//SE
	
//	BOOL			settingChanged;		//設定（音以外）変更があったか否か
	
	//BGM・SE関係
	NSURL			*bgmURL;
	AVAudioPlayer 	*bgmPlayer;
	
	NSURL			*tapURL;
	AVAudioPlayer	*tapPlayer;
	NSURL			*fanfareURL;
	AVAudioPlayer	*fanfarePlayer;
	NSURL			*consolationURL;
	AVAudioPlayer	*consolationPlayer;
	
	BOOL			playing;
	BOOL			interruptedOnPlayback;
	
	//タップしたときに音を出させないようにする。（初期配置でチップを並べるとき）
	BOOL			isValidSE;
	
	//タイマー用
	NSTimer			*displayTimer;		//表示用タイマー
	NSTimer			*decisionTimer;		//正解判定用タイマー
	NSTimer			*countDownTimer;	//カウントダウン用
	int				countDownNumber;	//3、2、1秒前の数字
	
	//Core Data 用
	NSManagedObjectContext 	*managedObjectContext;	    
	NSMutableArray 			*eventsArray;

	//IBが生成するメンバ変数 Outlet
	//ディスプレイエリア　（画面上部1/3)
	UILabel 		*questionTitleLabel;		//お題のタイトルラベル（ローカライズ用）
	UILabel 		*answerTitleLabel;			//回答欄のタイトルラベル（ローカライズ用）
	UILabel 		*timeDisplayLabel;			//残り時間を表示するためのラベル
	UIProgressView 	*timeProgressBar;			//残り時間表示用のプログレスバー
	UILabel 		*questionLabel;				//お題を表示するためのラベル
	UILabel 		*userInputDataDisplayLabel;	//現在入力中の回答を表示するためのラベル
	UILabel 		*operand1Label;				//第一被演算子表示用　（デバッグ用）
	UILabel 		*operand2Label;				//第一被演算子表示用　（デバッグ用）
	UIView 			*displayAreaColorView;		//画面上部1/3の色を変更するためのビュー
	UILabel 		*currentAnswer;				//現在入力中の回答の計算結果、表示用ラベル
	
	//吹き出し
	UIView *speechBalloonView;
	UILabel *speechBalloonLabel;
	UIImageView *speechBalloonImageView;
	
	
	
	
	
	//達成画面（ビュー）
	UIView 			*congratulationView;		//達成画面のビュー
	UIImageView 	*congratulationImageView;	//達成画面のイメージビュー
	UILabel 		*messageLabel;				//おめでとうなどのメッセージ表示用
	UILabel 		*phoneticLabel;				//オープニング時に日本語環境なら
												//「あんざんめいじん」と表示するためのラベル
	UILabel 		*newRecordLabel;			//新記録第○位ですと表示するためのラベル
	UILabel 		*recordTimeLabel;			//クリア時間表示用
	UILabel 		*closeLabel;				//「tap to close」表示用

	//チップ（ボタン）群をのせるためのビュー
	UIView 			*chipBackgroundView;

	//mask用
	UIView 			*maskView;					//ディスプレイエリアとチップエリアのマスク用
	UILabel 		*countDownLabel;
	UIView 			*maskViewForAll;			//全画面のマスク用　（カウントダウン用）
	
	//ガイダンス
	UIView *guidanceView;
	
	UIView *SBPlayView;
	UIImageView *SBPlayImageView;
	UILabel *SBPlayLabel;
	
	UIView *SBRecordView;
	UIImageView *SBRecordImageView;
	UILabel *SBRecordLabel;

	UIView *SBHelpView;
	UIImageView *SBHelpImageView;
	UILabel *SBHelpLabel;
	
	UIView *SBSettingsView;
	UIImageView *SBSettingsImageView;
	UILabel *SBSettingsLabel;
	
	UITextView *guidanceTextView;
	
	NSTimer *guidanceTimer;

	BOOL	guidanceFlag;
	BOOL 	openingImmediatelyLaterFlag;
	UIButton *guidanceCloseButton;

	
	//複数演算子確認用
	BOOL	pluralOperator;
	
}

//アプリケーションデリゲート
@property (retain, nonatomic) AppDelegate *applicationDelegate;

//プログラム内で使用するメンバ変数
@property (nonatomic, retain) 	NSDate 			*startTime;
@property (assign)			 	double 			remainingTime;
@property (assign) 				NSInteger 		questionNumber;
@property (assign) 				NSInteger 		answerNumber;
@property (nonatomic, retain) 	NSMutableArray 	*chipArray;
@property (nonatomic, retain) 	NSMutableArray	*needsChipList;
@property (nonatomic, retain)	NSMutableArray	*chipStack;

//設定のための変数
@property (assign) 				ArithmeticKind  arithmeticKind;
@property (assign) 				int 			digitOfNumbers;
@property (assign)				HelpLevel		helpLevel;
@property (assign) 				BOOL			backGroundMusic;
@property (assign)				BOOL			soundEffect;

//@property (assign)				BOOL			settingChanged;

//BGM・SE関係
@property (retain, nonatomic) NSURL 		*bgmURL;
@property (retain, nonatomic) AVAudioPlayer *bgmPlayer;

@property (retain, nonatomic) NSURL 		*tapURL;
@property (retain, nonatomic) AVAudioPlayer *tapPlayer;
@property (retain, nonatomic) NSURL 		*fanfareURL;
@property (retain, nonatomic) AVAudioPlayer *fanfarePlayer;
@property (retain, nonatomic) NSURL 		*consolationURL;
@property (retain, nonatomic) AVAudioPlayer *consolationPlayer;

@property (assign) BOOL playing;
@property (assign) BOOL interruptedOnPlayback;
			
//タップしたときに音を出させないようにする。（初期配置でチップを並べるとき）
@property (assign) BOOL isValidSE;

//タイマー用
//NSTimerに関しては不要
@property (assign) int 	countDownNumber;	//3、2、1秒前の数字


//Core Data 用
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain) NSMutableArray 		 *eventsArray;

//IBが生成するメンバ変数　Outlet
//ディスプレイエリア　（画面上部1/3)
@property (nonatomic, retain) IBOutlet UILabel 			*questionTitleLabel;
@property (nonatomic, retain) IBOutlet UILabel 			*answerTitleLabel;
@property (nonatomic, retain) IBOutlet UILabel 			*timeDisplayLabel;
@property (nonatomic, retain) IBOutlet UIProgressView 	*timeProgressBar;
@property (nonatomic, retain) IBOutlet UILabel 			*questionLabel;
@property (nonatomic, retain) IBOutlet UILabel 			*userInputDataDisplayLabel;
@property (nonatomic, retain) IBOutlet UILabel			*operand1Label;
@property (nonatomic, retain) IBOutlet UILabel 			*operand2Label;
@property (nonatomic, retain) IBOutlet UIView 			*displayAreaColorView;
@property (nonatomic, retain) IBOutlet UILabel 			*currentAnswer;

//達成画面（ビュー）
@property (nonatomic, retain) IBOutlet UIView 		*congratulationView;
@property (nonatomic, retain) IBOutlet UIImageView 	*congratulationImageView;
@property (nonatomic, retain) IBOutlet UILabel 		*messageLabel;
@property (nonatomic, retain) IBOutlet UILabel		*phoneticLabel;
@property (nonatomic, retain) IBOutlet UILabel 		*newRecordLabel;
@property (nonatomic, retain) IBOutlet UILabel 		*recordTimeLabel;
@property (nonatomic, retain) IBOutlet UILabel 		*closeLabel;

//チップ（ボタン）群をのせるためのビュー
@property (nonatomic, retain) IBOutlet UIView 		*chipBackgroundView;

//mask用
@property (nonatomic, retain) IBOutlet UIView 		*maskView;
@property (nonatomic, retain) IBOutlet UILabel 		*countDownLabel;
@property (nonatomic, retain) IBOutlet UIView 		*maskViewForAll;


//吹き出し用
@property (nonatomic, retain) IBOutlet UIView *speechBalloonView;
@property (nonatomic, retain) IBOutlet UILabel *speechBalloonLabel;
@property (nonatomic, retain) IBOutlet UIImageView *speechBalloonImageView;

//ガイダンス
@property (nonatomic, retain) IBOutlet UIView *guidanceView;

@property (nonatomic, retain) IBOutlet UIView *SBPlayView;
@property (nonatomic, retain) IBOutlet UIImageView *SBPlayImageView;
@property (nonatomic, retain) IBOutlet UILabel *SBPlayLabel;
@property (nonatomic, retain) IBOutlet UIImageView *imageView;


@property (nonatomic, retain) IBOutlet UIView *SBRecordView;
@property (nonatomic, retain) IBOutlet UIImageView *SBRecordImageView;
@property (nonatomic, retain) IBOutlet UILabel *SBRecordLabel;

@property (nonatomic, retain) IBOutlet UIView *SBHelpView;
@property (nonatomic, retain) IBOutlet UIImageView *SBHelpImageView;
@property (nonatomic, retain) IBOutlet UILabel *SBHelpLabel;

@property (nonatomic, retain) IBOutlet UIView *SBSettingsView;
@property (nonatomic, retain) IBOutlet UIImageView *SBSettingsImageView;
@property (nonatomic, retain) IBOutlet UILabel *SBSettingsLabel;

@property (nonatomic, retain) IBOutlet UITextView *guidanceTextView;

@property (assign) BOOL guidanceFlag;
@property (assign) BOOL openingImmediatelyLaterFlag;

@property (nonatomic, retain) IBOutlet UIButton *guidanceCloseButton;

//複数演算子確認用
@property (assign) BOOL	pluralOperator;


// action
- (IBAction)play:(id)sender;			//プレイボタンを押したとき
- (IBAction)showRecord:(id)sender;		//記録表示ボタンを押したとき
- (IBAction)showHelp:(id)sender;		//ヘルプボタンを押したとき
- (IBAction)showSetting:(id)sender;		//設定ボタンを押したとき
- (IBAction)backspaceKey:(id)sender; 	//バックスペースボタンを押したとき


- (IBAction)guidanceClose:(id)sender;

//メソッドの宣言（一部）
- (void) chipTouched:(UIButton *)button;
- (void) displayTimerStop;
- (void) decisionTimerStop;
- (void) chipsUserInteractionEnabled:(BOOL)yesOrNo;
- (void) chipAssignmentForOpeningAndSettingChanged:(BOOL)opening;
- (void) maskViewSetForUnTouched:(BOOL)yesOrNo;
- (void) maskViewSetForCountDown:(BOOL)yesOrNo;
- (BOOL) tooBig;

//開発中
int digit (int n);
- (void) addUnderBar;
- (NSString *) appendUnderBar:(NSString *)str number:(int)num;
- (NSString *) trimUnderBar:(NSString *)str;



@end
