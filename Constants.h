//
//  Constants.h
//  MathMaster
//
//  Created by 廣瀬 誠 on 平成23/11/28.
//  Copyright 23 岐阜IT協同組合. All rights reserved.
//

//MainView
#define CHIP_SIZE 	 45	//45px
#define MAP_SIZE 	  7	//画面は縦、横７つのチップで構成される。
#define CHIP_NUMBER	 49	//チップは全部で４９個ある。
#define VACANT 	(-1)	//未使用のつもり

#define UN_INPUTTED -1000000	//負の数の入力は許可していないし
								//引き算で負の数が発生しても最大6桁までなので、
								//−1000000であれば、num2が未入力の意味として使える

//RecordView
#define RECORD_MAX		10	//登録できる記録は、最大10件
#define RECORD_COLUMNS	 3	//time, digit, date, の3種類


//処理時間計測用
//P_START(someprocess);
// 処理いろいろ
//P_END(someprocess);
#define P_START(str) CFAbsoluteTime str##_processStartTime = CFAbsoluteTimeGetCurrent()
#define P_END(str) CFAbsoluteTime str##_processEndTime = CFAbsoluteTimeGetCurrent();\
NSLog(@"%s Proccess time: %f",#str, str##_processEndTime - str##_processStartTime);

typedef enum {
	ADDITION 		= 0,
	SUBTRACTION 	= 1,
	MULTIPLICATION 	= 2,
	DIVISION 		= 3,
} ArithmeticKind;

typedef enum {
	HARD 	= 2,
	NORMAL 	= 1,
	EASY 	= 0,
} HelpLevel;

typedef enum {
	OPENING = -1,
	TIMEUP 	= 0,
	
	SPRING 	= 1,
	SUMMER 	= 2,
	AUTUMN 	= 3,
	WINTER 	= 4,
} Season;



