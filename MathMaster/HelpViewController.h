//
//  HelpViewController.h
//  Mental ArithmeticKind Master
//
//  Created by 廣瀬 誠 on 平成23/11/10.
//  Copyright 23 岐阜IT協同組合. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HelpViewControllerDelegate;

@interface HelpViewController : UIViewController {
	UIButton *_helpButtonNounProject;
	UIButton *_helpButtonBeizGraphics;
	UIButton *_helpButtonYodoyabashi;
	UIButton *_helpButtonTabiPhoto;
	UIScrollView *_scrollView;
}

@property (nonatomic, retain) IBOutlet UIButton *helpButtonNounProject;
@property (nonatomic, retain) IBOutlet UIButton *helpButtonBeizGraphics;
@property (nonatomic, retain) IBOutlet UIButton *helpButtonYodoyabashi;
@property (nonatomic, retain) IBOutlet UIButton *helpButtonTabiPhoto;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;



@property (nonatomic, assign) id <HelpViewControllerDelegate> delegate;

- (IBAction)back:(id)sender;

- (IBAction)buttonNounProject:(id)sender;
- (IBAction)buttonBeizGraphics:(id)sender;
- (IBAction)buttonYodoyabashi:(id)sender;
- (IBAction)buttonTabiPhoto:(id)sender;

@end

@protocol HelpViewControllerDelegate
- (void)helpViewControllerDidFinish:(HelpViewController *)controller;
@end

/*
 << 著作権等 >>
 以下のサイトのアイコン・写真を用いて作成いたしました。感謝いたします。
 
 アイコン：
 The Noun Project
 http://thenounproject.com/
 
 写真：
 BEIZ Graphics Web
 http://www.beiz.jp/
 
 淀屋橋心理療法センター
 http://www.yodoyabashift.com/
 
 フリー写真素材集 旅Photo
 http://photo.yu-travel.net/
*/