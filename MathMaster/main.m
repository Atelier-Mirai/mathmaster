//
//  main.m
//  MathMaster
//
//  Created by 廣瀬 誠 on 平成23/11/18.
//  Copyright 23 岐阜IT協同組合. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[])
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	int retVal = UIApplicationMain(argc, argv, nil, nil);
	[pool release];
	return retVal;
}
