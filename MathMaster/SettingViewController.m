//
//  SettingViewController.m
//  Mental ArithmeticKind Master
//
//  Created by 廣瀬 誠 on 平成23/11/17.
//  Copyright 23 岐阜IT協同組合. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "SettingViewController.h"



@implementation SettingViewController

//設定値を保持する変数
@synthesize kind;
@synthesize digit;
@synthesize helpLevel;
@synthesize bgm;
@synthesize se;

//設定変更したか否か
@synthesize settingChanged;

//IB Outlet	
@synthesize kindSegmentedControl;
@synthesize digitSegmentedControl;

@synthesize hardButton;
@synthesize normalButton;
@synthesize easyButton;

@synthesize bgmSwitch;
@synthesize seSwitch;


@synthesize delegate=_delegate;


//ボタンが選ばれているときに、色をつけるメソッド
- (void) setButtonHighLight {
	
	UIColor *hardColor = [UIColor colorWithRed:1 green:0 blue:0.5 alpha:1];
	UIColor *normalColor = [UIColor colorWithRed:0 green:0.25 blue:0.5 alpha:1];
	UIColor *easyColor = [UIColor colorWithRed:0 green:0.5 blue:0 alpha:1];
	
	hardButton.layer.masksToBounds = YES;
	hardButton.layer.cornerRadius = 10;
	normalButton.layer.masksToBounds = YES;
	normalButton.layer.cornerRadius = 10;
	easyButton.layer.masksToBounds = YES;
	easyButton.layer.cornerRadius = 10;
	
	switch (helpLevel) {
		case HARD:
			[hardButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
			[hardButton setBackgroundImage:[UIImage imageNamed:@"highlight.png"] forState:UIControlStateNormal];
			
			[normalButton setTitleColor:normalColor forState:UIControlStateNormal];
			[normalButton setBackgroundImage:[UIImage imageNamed:@"white.png"] forState:UIControlStateNormal];
			
			[easyButton setTitleColor:easyColor forState:UIControlStateNormal];
			[easyButton setBackgroundImage:[UIImage imageNamed:@"white.png"] forState:UIControlStateNormal];
			break;
		case NORMAL:
			[hardButton setTitleColor:hardColor forState:UIControlStateNormal];
			[hardButton setBackgroundImage:[UIImage imageNamed:@"white.png"] forState:UIControlStateNormal];
			
			[normalButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
			[normalButton setBackgroundImage:[UIImage imageNamed:@"highlight.png"] forState:UIControlStateNormal];
			
			[easyButton setTitleColor:easyColor forState:UIControlStateNormal];
			[easyButton setBackgroundImage:[UIImage imageNamed:@"white.png"] forState:UIControlStateNormal];
			break;
		case EASY:
			[hardButton setTitleColor:hardColor forState:UIControlStateNormal];
			[hardButton setBackgroundImage:[UIImage imageNamed:@"white.png"] forState:UIControlStateNormal];
			
			[normalButton setTitleColor:normalColor forState:UIControlStateNormal];
			[normalButton setBackgroundImage:[UIImage imageNamed:@"white.png"] forState:UIControlStateNormal];
			
			[easyButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
			[easyButton setBackgroundImage:[UIImage imageNamed:@"highlight.png"] forState:UIControlStateNormal];
			break;
	}
	
}



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
	
	//IB Outlet	
	[kindSegmentedControl release];
	[digitSegmentedControl release];
	
	[hardButton release];
	[normalButton release];
	[easyButton release];
    
	[bgmSwitch release];
    [seSwitch release];
   	[super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - userDefault

- (void)saveUserDefault {
	//演算の種類、使用する数の桁数をユーザーデフォルトに書き込む
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	[userDefaults setInteger:kind forKey:@"ARITHMETIC_KIND"];
	[userDefaults setInteger:digit forKey:@"DIGIT_OF_NUMBERS"];
	[userDefaults setInteger:helpLevel forKey:@"HELP_LEVEL"];
	[userDefaults setBool:bgm forKey:@"BGM"];
	[userDefaults setBool:se forKey:@"SE"];
	
	[userDefaults synchronize];
}

- (void)loadUserDefault {
	//演算の種類、使用する数の桁数をユーザーデフォルトから読み込む
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	kind = [userDefaults integerForKey:@"ARITHMETIC_KIND"];
	digit = [userDefaults integerForKey:@"DIGIT_OF_NUMBERS"];
	helpLevel = [userDefaults integerForKey:@"HELP_LEVEL"];
	bgm = [userDefaults boolForKey:@"BGM"];
	se = [userDefaults boolForKey:@"SE"];
}


#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view from its nib.
	
	//初期値設定
	//ユーザーデフォルトから読み込む
	[self loadUserDefault];
	//読み込んだ値を元に、セグメントコントロールを選択済みにする。
	kindSegmentedControl.selectedSegmentIndex = kind;
	digitSegmentedControl.selectedSegmentIndex = digit-1;
	
	//ボタンを選択済みにする　＆　表示を青色にしてハイライトしているように見せる。
	[self setButtonHighLight];
	
	//スイッチの状態をセット
	bgmSwitch.on = bgm;
	seSwitch.on = se;
	
	//設定（音以外）が変更されたか否か？
	//変更されたら、配置されているチップを変更する。
	settingChanged = NO;
	
}

- (void)viewDidUnload
{
	//IB Outlet	
	[self setKindSegmentedControl:nil];
	[self setDigitSegmentedControl:nil];
	
	[self setHardButton:nil];
	[self setNormalButton:nil];
	[self setEasyButton:nil];
	[self setBgmSwitch:nil];
	[self setSeSwitch:nil];

    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)back:(id)sender {
	
	//ユーザーデフォルトに保存する。
	[self saveUserDefault];

	//メインに戻る
	[self.delegate settingViewControllerDidFinish:self
								   settingChanged:settingChanged];

}



- (IBAction)bgmSwitchAction:(id)sender {
	UISwitch *mySwitch = (UISwitch *)sender;
	//bgm = mySwitch.on;と等価だけれど、分かりやすさのため
	if (mySwitch.on){
		bgm = YES;
	} else {
		bgm = NO;
	}
	
}

- (IBAction)seSwitchAction:(id)sender {
	UISwitch *mySwitch = (UISwitch *)sender;
	if (mySwitch.on){
		se = YES;
	} else {
		se = NO;
	}
}

- (IBAction)selectArithmeticKind:(id)sender {
	UISegmentedControl *mySegmentControl = sender;
	kind = mySegmentControl.selectedSegmentIndex;
	
	//ここにきているということは、設定が変わったということなので。
	settingChanged = YES;
	
}

- (IBAction)selectDigitOfNumbers:(id)sender {
	UISegmentedControl *mySegmentControl = sender;
	digit = mySegmentControl.selectedSegmentIndex + 1;


	//ここにきているということは、設定が変わったということなので。
	settingChanged = YES;

}


- (IBAction)helpLevelSetting:(id)sender {
	UIButton *button = (UIButton*)sender;
	
	if (button.tag != helpLevel) {
		//ヘルプレベルの変更があったということなので、
		helpLevel = button.tag;
		settingChanged = YES;
	}
	
	[self setButtonHighLight];

}
@end
